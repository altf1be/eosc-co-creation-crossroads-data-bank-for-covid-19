# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.4.241](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/compare/v3.4.240...v3.4.241) (2021-05-06)


### Documentations

* update with Weather and UV-Index for May 05, 2021 ([55cbd21](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/commit/55cbd211da574ae8c009689526cf6f411863af13))

### [3.4.240](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/compare/v3.4.239...v3.4.240) (2021-05-02)


### Documentations

* update with Weather and UV-Index for May 01, 2021 ([282b1e9](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/commit/282b1e990a7b9460a79809e4e75f52cb842a3b5e))

### [3.4.239](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/compare/v3.4.238...v3.4.239) (2021-04-28)


### Documentations

* update with Weather and UV-Index for April 27, 2021 ([9ce6011](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/commit/9ce601168d3c83edf814606a62add092d7224b39))

### [3.4.238](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/compare/v3.4.237...v3.4.238) (2021-04-27)


### Documentations

* add the acknowledgement that we received a funding from EOSC Secretariat ([41627a1](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/commit/41627a140d73f12f4304df8e209986f176071d6f))
* **fix:** the file is not stored ([f8c0e9f](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/commit/f8c0e9f1a3f3479139821d5dba1e499793d92d55))

### [3.4.237](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/compare/v3.4.236...v3.4.237) (2021-04-27)


### Documentations

* update with Weather and UV-Index for April 26, 2021 ([522a74f](https://github.com/ALT-F1/eosc-co-creation-crossroads-data-bank-for-covid-19/commit/522a74fa05e41c740323b3c4d48483d4f28f439a))

### [3.4.236](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.235...v3.4.236) (2021-04-26)


### Documentations

* update with Weather and UV-Index for April 25, 2021 ([d303081](https://github.com/ALT-F1/OpenWeatherMap/commit/d30308142b5ffca6613b3e9f5e5962ef58966ef9))

### [3.4.235](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.234...v3.4.235) (2021-04-25)


### Documentations

* update with Weather and UV-Index for April 24, 2021 ([b7787cd](https://github.com/ALT-F1/OpenWeatherMap/commit/b7787cd9fbc8fe07ea2f559d5733b67f6bc13ed9))

### [3.4.234](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.233...v3.4.234) (2021-04-24)


### Documentations

* update with Weather and UV-Index for April 23, 2021 ([dfee8af](https://github.com/ALT-F1/OpenWeatherMap/commit/dfee8afb860653d12c6b0c76f372572f12d82312))

### [3.4.233](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.232...v3.4.233) (2021-04-23)


### Documentations

* update with Weather and UV-Index for April 22, 2021 ([1c227ba](https://github.com/ALT-F1/OpenWeatherMap/commit/1c227badf2bb67699671c176132024fea6f6f8be))

### [3.4.232](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.231...v3.4.232) (2021-04-21)


### Documentations

* update with Weather and UV-Index for April 20, 2021 ([4d94409](https://github.com/ALT-F1/OpenWeatherMap/commit/4d94409d786ee513c0fb99ce0fbffacf4dcb481c))

### [3.4.231](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.230...v3.4.231) (2021-04-20)


### Documentations

* update with Weather and UV-Index for April 19, 2021 ([2ac5282](https://github.com/ALT-F1/OpenWeatherMap/commit/2ac5282e0c22c56963569df3ea945f13f1d63154))

### [3.4.230](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.229...v3.4.230) (2021-04-19)


### Documentations

* update with Weather and UV-Index for April 18, 2021 ([9d0c73b](https://github.com/ALT-F1/OpenWeatherMap/commit/9d0c73b40f735322dbb54e8b4dafe22210698905))

### [3.4.229](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.228...v3.4.229) (2021-04-18)


### Documentations

* update with Weather and UV-Index for April 17, 2021 ([ad6cec5](https://github.com/ALT-F1/OpenWeatherMap/commit/ad6cec599be1fdc890ae38d994119b1fc98baa69))

### [3.4.228](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.227...v3.4.228) (2021-04-17)


### Documentations

* update with Weather and UV-Index for April 16, 2021 ([595bbf4](https://github.com/ALT-F1/OpenWeatherMap/commit/595bbf4df7d7cd77ec1518db6758867a0e660178))

### [3.4.227](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.226...v3.4.227) (2021-04-16)


### Documentations

* update with Weather and UV-Index for April 15, 2021 ([e032125](https://github.com/ALT-F1/OpenWeatherMap/commit/e032125682d6fcd66bc21aac679d99832d1f9f6d))

### [3.4.226](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.225...v3.4.226) (2021-04-14)


### Documentations

* update with Weather and UV-Index for April 13, 2021 ([ebeadf0](https://github.com/ALT-F1/OpenWeatherMap/commit/ebeadf02e4ad208f6258ed416e75bcff9ddf4901))

### [3.4.225](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.224...v3.4.225) (2021-04-13)


### Documentations

* update with Weather and UV-Index for April 01, 2021 ([752afd2](https://github.com/ALT-F1/OpenWeatherMap/commit/752afd2938ec4f163cc15af80860bcc472e3477a))
* update with Weather and UV-Index for April 02, 2021 ([48c4882](https://github.com/ALT-F1/OpenWeatherMap/commit/48c4882df7dbff9d4a71f3dbf20d6f34bac2d7d2))
* update with Weather and UV-Index for April 03, 2021 ([0150d0a](https://github.com/ALT-F1/OpenWeatherMap/commit/0150d0a322d263c0440a2505db19c17122ff89f1))
* update with Weather and UV-Index for April 04, 2021 ([17a7946](https://github.com/ALT-F1/OpenWeatherMap/commit/17a7946855221a8e0c853c98ebcc554121bfa961))
* update with Weather and UV-Index for April 05, 2021 ([5514d8e](https://github.com/ALT-F1/OpenWeatherMap/commit/5514d8e68b6b97b276fe0879a411900028a504c8))
* update with Weather and UV-Index for April 06, 2021 ([667ed58](https://github.com/ALT-F1/OpenWeatherMap/commit/667ed586c75f5d40f4c245d27942d9ff40ffca97))
* update with Weather and UV-Index for April 07, 2021 ([92ca941](https://github.com/ALT-F1/OpenWeatherMap/commit/92ca9416f94dd3659135eea62e7fa92d076b0922))
* update with Weather and UV-Index for April 08, 2021 ([12b872d](https://github.com/ALT-F1/OpenWeatherMap/commit/12b872d256180dcfc3da40b6f1c6b38e2898bbb5))
* update with Weather and UV-Index for April 10, 2021 ([607a7ed](https://github.com/ALT-F1/OpenWeatherMap/commit/607a7ed7f67092325be307f0b160d116dd077847))
* update with Weather and UV-Index for April 11, 2021 ([7679975](https://github.com/ALT-F1/OpenWeatherMap/commit/7679975dcb618501d696d6176bee49d922a73b81))
* update with Weather and UV-Index for April 12, 2021 ([a8dc07d](https://github.com/ALT-F1/OpenWeatherMap/commit/a8dc07db4c851092f8ef8133ef86f468710ed34f))
* update with Weather and UV-Index for March 28, 2021 ([f9f0f93](https://github.com/ALT-F1/OpenWeatherMap/commit/f9f0f937e0df5cd24a8dde9da9c979d06acb628f))
* update with Weather and UV-Index for March 29, 2021 ([1fc5222](https://github.com/ALT-F1/OpenWeatherMap/commit/1fc5222f81590638fe6470b22bdc64f501725c0b))
* update with Weather and UV-Index for March 30, 2021 ([bd446f1](https://github.com/ALT-F1/OpenWeatherMap/commit/bd446f10b1045ebdac92cc1ef3ae4465fa7cbcec))
* update with Weather and UV-Index for March 31, 2021 ([4d50f18](https://github.com/ALT-F1/OpenWeatherMap/commit/4d50f18d8bba9a07e2b8e53fa8b857dfa757560c))

### [3.4.224](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.223...v3.4.224) (2021-03-28)


### Documentations

* update with Weather and UV-Index for March 27, 2021 ([2d3e723](https://github.com/ALT-F1/OpenWeatherMap/commit/2d3e723066f70edb8ba91bb8cef46aabefa12bab))

### [3.4.223](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.222...v3.4.223) (2021-03-26)


### Documentations

* update with Weather and UV-Index for March 25, 2021 ([9cbe7a8](https://github.com/ALT-F1/OpenWeatherMap/commit/9cbe7a862508cbbaf5b8ab1a927de6d1c8958a9a))

### [3.4.222](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.221...v3.4.222) (2021-03-25)


### Documentations

* update with Weather and UV-Index for March 24, 2021 ([a07477c](https://github.com/ALT-F1/OpenWeatherMap/commit/a07477c6cdc4868f0ddf97f72d4dd3a339c7f39f))

### [3.4.221](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.220...v3.4.221) (2021-03-24)


### Documentations

* update with Weather and UV-Index for March 23, 2021 ([6c68bb8](https://github.com/ALT-F1/OpenWeatherMap/commit/6c68bb80c6c82589b106adfd9a55842abc83c095))

### [3.4.220](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.219...v3.4.220) (2021-03-23)


### Documentations

* update with Weather and UV-Index for March 22, 2021 ([a0bee6b](https://github.com/ALT-F1/OpenWeatherMap/commit/a0bee6b66b4bebd804101350e0474496c14598ba))

### [3.4.219](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.218...v3.4.219) (2021-03-21)


### Documentations

* update with Weather and UV-Index for March 20, 2021 ([b6f1f68](https://github.com/ALT-F1/OpenWeatherMap/commit/b6f1f68dc82bcf1dca798b0e572f54942b2ac893))

### [3.4.218](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.217...v3.4.218) (2021-03-19)


### Documentations

* update with Weather and UV-Index for March 18, 2021 ([a94d139](https://github.com/ALT-F1/OpenWeatherMap/commit/a94d13903ea05880e8f6df522b1b0737e9ec634f))

### [3.4.217](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.216...v3.4.217) (2021-03-17)


### Documentations

* update with Weather and UV-Index for March 16, 2021 ([8f56bfe](https://github.com/ALT-F1/OpenWeatherMap/commit/8f56bfe0ab062431a8bc4e7c762d807d3b480b66))

### [3.4.216](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.215...v3.4.216) (2021-03-16)


### Documentations

* update with Weather and UV-Index for March 15, 2021 ([a0d84d3](https://github.com/ALT-F1/OpenWeatherMap/commit/a0d84d3f27d99eae4ad4f70be0925431936fdc9b))

### [3.4.215](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.214...v3.4.215) (2021-03-15)


### Documentations

* update with Weather and UV-Index for March 14, 2021 ([486683c](https://github.com/ALT-F1/OpenWeatherMap/commit/486683cdc67e2321da2ba2c0132aa0225cf262ec))

### [3.4.214](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.213...v3.4.214) (2021-03-14)


### Documentations

* update with Weather and UV-Index for March 13, 2021 ([c542d30](https://github.com/ALT-F1/OpenWeatherMap/commit/c542d305f2b6ffa85844fab7e277d770a93dbc05))

### [3.4.213](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.212...v3.4.213) (2021-03-13)


### Documentations

* update with Weather and UV-Index for March 12, 2021 ([b69c8a9](https://github.com/ALT-F1/OpenWeatherMap/commit/b69c8a94547f078c448691edd081a3426440c306))

### [3.4.212](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.211...v3.4.212) (2021-03-12)


### Documentations

* update with Weather and UV-Index for March 11, 2021 ([ae6795b](https://github.com/ALT-F1/OpenWeatherMap/commit/ae6795b2963997fca9daaec1116b207e40f9337f))

### [3.4.211](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.210...v3.4.211) (2021-03-11)

### [3.4.210](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.209...v3.4.210) (2021-03-11)


### Documentations

* update with Weather and UV-Index for March 10, 2021 ([d17fdc6](https://github.com/ALT-F1/OpenWeatherMap/commit/d17fdc6a2e3b8b955256c20d7170ad01e9ace7f2))

### [3.4.209](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.208...v3.4.209) (2021-03-09)


### Documentations

* update with Weather and UV-Index for March 08, 2021 ([4cb9c1d](https://github.com/ALT-F1/OpenWeatherMap/commit/4cb9c1ddd256370626c1238de15515308942fe65))

### [3.4.208](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.207...v3.4.208) (2021-03-08)


### Documentations

* update with Weather and UV-Index for March 07, 2021 ([b8b02bf](https://github.com/ALT-F1/OpenWeatherMap/commit/b8b02bf3794ff1f4c61c97de9ffffed872bff3ae))

### [3.4.207](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.206...v3.4.207) (2021-03-07)


### Documentations

* update with Weather and UV-Index for March 06, 2021 ([71d6b71](https://github.com/ALT-F1/OpenWeatherMap/commit/71d6b716ddcc8c8d00f00f64ea526935407988ee))

### [3.4.206](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.205...v3.4.206) (2021-03-06)


### Documentations

* update with Weather and UV-Index for March 05, 2021 ([e3dfee2](https://github.com/ALT-F1/OpenWeatherMap/commit/e3dfee216cd1c2e03e9afaa19adcc510a2c3b886))

### [3.4.205](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.204...v3.4.205) (2021-03-04)


### Documentations

* update with Weather and UV-Index for March 03, 2021 ([9052252](https://github.com/ALT-F1/OpenWeatherMap/commit/90522524d859af962ff6191ed518608504f9e791))

### [3.4.204](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.203...v3.4.204) (2021-03-03)


### Documentations

* update with Weather and UV-Index for March 02, 2021 ([327d8bc](https://github.com/ALT-F1/OpenWeatherMap/commit/327d8bc503c5392a3548ae16215f5eed19db14ae))

### [3.4.203](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.202...v3.4.203) (2021-03-02)


### Documentations

* update with Weather and UV-Index for March 01, 2021 ([61ba7c4](https://github.com/ALT-F1/OpenWeatherMap/commit/61ba7c45b0a3cbce5737e02828cbcb422f312f78))

### [3.4.202](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.201...v3.4.202) (2021-03-01)


### Documentations

* update with Weather and UV-Index for February 28, 2021 ([62810c0](https://github.com/ALT-F1/OpenWeatherMap/commit/62810c0b898a5328845eed18050d1a28cabfe8d9))

### [3.4.201](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.200...v3.4.201) (2021-02-28)


### Documentations

* update with Weather and UV-Index for February 27, 2021 ([4113332](https://github.com/ALT-F1/OpenWeatherMap/commit/411333216d3603c5c4bc17c3bb510395e2c070a8))

### [3.4.200](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.199...v3.4.200) (2021-02-27)


### Documentations

* update with Weather and UV-Index for February 26, 2021 ([1572c12](https://github.com/ALT-F1/OpenWeatherMap/commit/1572c12807c68abc1479b894af91a7597e7b8132))

### [3.4.199](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.198...v3.4.199) (2021-02-26)


### Documentations

* update with Weather and UV-Index for February 25, 2021 ([682627a](https://github.com/ALT-F1/OpenWeatherMap/commit/682627a0bfc79fdcde4c89af47eb469df1062f60))

### [3.4.198](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.197...v3.4.198) (2021-02-25)


### Documentations

* update with Weather and UV-Index for February 24, 2021 ([d425b3d](https://github.com/ALT-F1/OpenWeatherMap/commit/d425b3d03edbf381dc5f4321e21ae3accbc17bdc))

### [3.4.197](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.196...v3.4.197) (2021-02-24)


### Documentations

* update with Weather and UV-Index for February 23, 2021 ([aeffb57](https://github.com/ALT-F1/OpenWeatherMap/commit/aeffb575df5594b07a1ebfcacf40bc467eae807d))

### [3.4.196](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.195...v3.4.196) (2021-02-23)


### Documentations

* update with Weather and UV-Index for February 22, 2021 ([cba558f](https://github.com/ALT-F1/OpenWeatherMap/commit/cba558f91d9b0f6d321eee4440dc2d472a3356f2))

### [3.4.195](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.194...v3.4.195) (2021-02-22)


### Documentations

* update with Weather and UV-Index for February 21, 2021 ([7cb8bb5](https://github.com/ALT-F1/OpenWeatherMap/commit/7cb8bb5e19d414b26a1b93ce9b0d32f9c1a798d6))

### [3.4.194](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.193...v3.4.194) (2021-02-21)


### Documentations

* update with Weather and UV-Index for February 20, 2021 ([06f5f15](https://github.com/ALT-F1/OpenWeatherMap/commit/06f5f1577e3fc43e126fe5d5975b248d90bc283b))

### [3.4.193](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.192...v3.4.193) (2021-02-20)


### Documentations

* update with Weather and UV-Index for February 19, 2021 ([153982a](https://github.com/ALT-F1/OpenWeatherMap/commit/153982a661195b8938f629ba577dc79fe8c22e76))

### [3.4.192](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.191...v3.4.192) (2021-02-19)


### Documentations

* update with Weather and UV-Index for February 18, 2021 ([97460ad](https://github.com/ALT-F1/OpenWeatherMap/commit/97460ad0322145f0c6fd7fee851bb62f6222dcd7))

### [3.4.191](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.190...v3.4.191) (2021-02-19)

### [3.4.190](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.189...v3.4.190) (2021-02-18)


### Documentations

* update with Weather and UV-Index for February 17, 2021 ([fcc8465](https://github.com/ALT-F1/OpenWeatherMap/commit/fcc8465b9ebcb0c9b6eabf4c4152fb279aadcdb5))

### [3.4.189](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.188...v3.4.189) (2021-02-17)


### Documentations

* update with Weather and UV-Index for February 16, 2021 ([1a62b89](https://github.com/ALT-F1/OpenWeatherMap/commit/1a62b8982c1b5c5ed2dd19c2857ce29e35c064a5))

### [3.4.188](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.187...v3.4.188) (2021-02-16)


### Documentations

* update with Weather and UV-Index for February 15, 2021 ([01d5e46](https://github.com/ALT-F1/OpenWeatherMap/commit/01d5e4659887e39f451eeb0064cd554a0a1920c5))

### [3.4.187](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.186...v3.4.187) (2021-02-15)


### Documentations

* update with Weather and UV-Index for February 14, 2021 ([c464757](https://github.com/ALT-F1/OpenWeatherMap/commit/c46475780e8705d12e1d65c9a8769ddd918f86fa))

### [3.4.186](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.185...v3.4.186) (2021-02-14)


### Documentations

* update with Weather and UV-Index for February 13, 2021 ([9ec53e4](https://github.com/ALT-F1/OpenWeatherMap/commit/9ec53e46cae50216b19fdd9b70ab285dc760b0e5))

### [3.4.185](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.184...v3.4.185) (2021-02-13)


### Documentations

* update with Weather and UV-Index for February 12, 2021 ([e92e53e](https://github.com/ALT-F1/OpenWeatherMap/commit/e92e53eecc3746bea95453dfc28eb2f18467cac0))

### [3.4.184](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.183...v3.4.184) (2021-02-12)


### Documentations

* update with Weather and UV-Index for February 11, 2021 ([365f3ff](https://github.com/ALT-F1/OpenWeatherMap/commit/365f3ff7d9fe3db100d38dfcf40686353ec82b05))

### [3.4.183](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.182...v3.4.183) (2021-02-11)


### Documentations

* update with Weather and UV-Index for February 10, 2021 ([de566d6](https://github.com/ALT-F1/OpenWeatherMap/commit/de566d6fbd14e3999869d495d02f385b7c12b819))

### [3.4.182](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.181...v3.4.182) (2021-02-10)


### Documentations

* update with Weather and UV-Index for February 09, 2021 ([59427f3](https://github.com/ALT-F1/OpenWeatherMap/commit/59427f35e00844f104bfd157d8754b06c5f49eb1))

### [3.4.181](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.180...v3.4.181) (2021-02-09)


### Documentations

* update with Weather and UV-Index for February 08, 2021 ([78280ca](https://github.com/ALT-F1/OpenWeatherMap/commit/78280ca04df4ee032e7c72c971bd6535a0766f90))

### [3.4.180](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.179...v3.4.180) (2021-02-08)


### Documentations

* update with Weather and UV-Index for February 07, 2021 ([beb5f5f](https://github.com/ALT-F1/OpenWeatherMap/commit/beb5f5fe52dcb731fe99997a6f42d86ef16a511a))

### [3.4.179](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.178...v3.4.179) (2021-02-07)


### Documentations

* update with Weather and UV-Index for February 06, 2021 ([cca56fb](https://github.com/ALT-F1/OpenWeatherMap/commit/cca56fb178f9c2f9239ea2ee1d5574058157c227))

### [3.4.178](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.177...v3.4.178) (2021-02-05)


### Documentations

* update with Weather and UV-Index for February 04, 2021 ([08c48b4](https://github.com/ALT-F1/OpenWeatherMap/commit/08c48b47e286c32fa353d001e942e7577b605387))

### [3.4.177](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.176...v3.4.177) (2021-02-04)


### Documentations

* update with Weather and UV-Index for February 03, 2021 ([e1fde44](https://github.com/ALT-F1/OpenWeatherMap/commit/e1fde44b9720d7b863eaf75564b378f6e21ac620))

### [3.4.176](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.175...v3.4.176) (2021-02-03)


### Documentations

* update with Weather and UV-Index for February 02, 2021 ([6d3a6b5](https://github.com/ALT-F1/OpenWeatherMap/commit/6d3a6b5865918aca640b9e708888265573c75f17))

### [3.4.175](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.174...v3.4.175) (2021-02-02)


### Documentations

* update with Weather and UV-Index for February 01, 2021 ([43e45b7](https://github.com/ALT-F1/OpenWeatherMap/commit/43e45b73bc146d411172c5df54cc4a8838c58719))

### [3.4.174](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.173...v3.4.174) (2021-02-01)


### Documentations

* update with Weather and UV-Index for January 31, 2021 ([1d67e02](https://github.com/ALT-F1/OpenWeatherMap/commit/1d67e02cc36f8851322d6b254a1dc389fcb8414d))

### [3.4.173](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.172...v3.4.173) (2021-01-31)


### Documentations

* update with Weather and UV-Index for January 30, 2021 ([3b426d4](https://github.com/ALT-F1/OpenWeatherMap/commit/3b426d4bd6095b408a95ecf30f02b0c725478b72))

### [3.4.172](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.171...v3.4.172) (2021-01-30)


### Documentations

* update with Weather and UV-Index for January 29, 2021 ([a602650](https://github.com/ALT-F1/OpenWeatherMap/commit/a602650ca7625c140be3e860ee92e55eca0f95af))

### [3.4.171](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.170...v3.4.171) (2021-01-29)


### Documentations

* update with Weather and UV-Index for January 28, 2021 ([6112641](https://github.com/ALT-F1/OpenWeatherMap/commit/611264142767f2055b5a5b41a99b02db470798bd))

### [3.4.170](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.169...v3.4.170) (2021-01-28)


### Documentations

* update with Weather and UV-Index for January 27, 2021 ([2543390](https://github.com/ALT-F1/OpenWeatherMap/commit/254339002b3dbbd0783ce2a067a5dba38f97b6e8))

### [3.4.169](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.168...v3.4.169) (2021-01-27)


### Documentations

* update with Weather and UV-Index for January 26, 2021 ([b55f423](https://github.com/ALT-F1/OpenWeatherMap/commit/b55f423ac3a80c28bb41966d1100caf219476667))

### [3.4.168](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.167...v3.4.168) (2021-01-26)


### Documentations

* update with Weather and UV-Index for January 25, 2021 ([6286cc1](https://github.com/ALT-F1/OpenWeatherMap/commit/6286cc160481687acbcd26f5f074e9f95af7a84f))

### [3.4.167](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.166...v3.4.167) (2021-01-25)


### Documentations

* update with Weather and UV-Index for January 24, 2021 ([28f8d24](https://github.com/ALT-F1/OpenWeatherMap/commit/28f8d24bed71f37dcf12dd68613a68c515ee2054))

### [3.4.166](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.165...v3.4.166) (2021-01-23)


### Documentations

* update with Weather and UV-Index for January 22, 2021 ([7c988e6](https://github.com/ALT-F1/OpenWeatherMap/commit/7c988e63bfe56a116bbb64918c2b76ac96ef6b69))

### [3.4.165](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.164...v3.4.165) (2021-01-22)


### Documentations

* update with Weather and UV-Index for January 21, 2021 ([b111c6b](https://github.com/ALT-F1/OpenWeatherMap/commit/b111c6b3173e968e347ca5c652b26c9faa382f53))

### [3.4.164](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.163...v3.4.164) (2021-01-21)


### Documentations

* update with Weather and UV-Index for January 20, 2021 ([f3d695d](https://github.com/ALT-F1/OpenWeatherMap/commit/f3d695dbdc1d4e991a7f3155c62fd7dfe6c32b8e))

### [3.4.163](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.162...v3.4.163) (2021-01-20)


### Documentations

* update with Weather and UV-Index for January 19, 2021 ([210c9d4](https://github.com/ALT-F1/OpenWeatherMap/commit/210c9d401f585ac20d956a5f5c74bb7a34c26c5b))

### [3.4.162](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.161...v3.4.162) (2021-01-19)


### Documentations

* update with Weather and UV-Index for January 18, 2021 ([b2e1182](https://github.com/ALT-F1/OpenWeatherMap/commit/b2e1182bbd5b5187bf1e2fc5266f8b61725d302d))

### [3.4.161](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.160...v3.4.161) (2021-01-18)


### Documentations

* update with Weather and UV-Index for January 17, 2021 ([b8979e1](https://github.com/ALT-F1/OpenWeatherMap/commit/b8979e1e97e75a016b7d7e54ce3d348b8c735829))

### [3.4.160](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.159...v3.4.160) (2021-01-17)


### Documentations

* update with Weather and UV-Index for January 16, 2021 ([0c12707](https://github.com/ALT-F1/OpenWeatherMap/commit/0c12707d206033de9a2ee28a3baa6f55e5ad3c51))

### [3.4.159](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.158...v3.4.159) (2021-01-16)


### Documentations

* update with Weather and UV-Index for January 15, 2021 ([107de75](https://github.com/ALT-F1/OpenWeatherMap/commit/107de753550fa4e33cb217a0cf0f125f607b28a2))

### [3.4.158](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.157...v3.4.158) (2021-01-15)


### Documentations

* update with Weather and UV-Index for January 14, 2021 ([5a62619](https://github.com/ALT-F1/OpenWeatherMap/commit/5a62619a84576c4429adf16c03ecfeed1c2e1594))

### [3.4.157](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.156...v3.4.157) (2021-01-14)


### Documentations

* update with Weather and UV-Index for January 13, 2021 ([872c13d](https://github.com/ALT-F1/OpenWeatherMap/commit/872c13d1476ad60a15b33d65f6ab9c820a698f0a))

### [3.4.156](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.155...v3.4.156) (2021-01-13)


### Documentations

* update with Weather and UV-Index for January 12, 2021 ([b46e254](https://github.com/ALT-F1/OpenWeatherMap/commit/b46e254053b443fcaac03e1c3b2bf17789e917bc))

### [3.4.155](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.154...v3.4.155) (2021-01-12)


### Documentations

* update with Weather and UV-Index for January 11, 2021 ([adc6c42](https://github.com/ALT-F1/OpenWeatherMap/commit/adc6c42a88dc9ab5095c2350b262c884e4e85676))

### [3.4.154](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.153...v3.4.154) (2021-01-11)


### Documentations

* update with Weather and UV-Index for January 10, 2021 ([3bc84b5](https://github.com/ALT-F1/OpenWeatherMap/commit/3bc84b5046e45e6378be87913fb66e76baf08c7a))

### [3.4.153](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.152...v3.4.153) (2021-01-10)


### Documentations

* update with Weather and UV-Index for January 09, 2021 ([a563cfd](https://github.com/ALT-F1/OpenWeatherMap/commit/a563cfd4b35da73920e62e17405fdd87d68af701))

### [3.4.152](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.151...v3.4.152) (2021-01-09)


### Documentations

* update with Weather and UV-Index for January 08, 2021 ([12bae64](https://github.com/ALT-F1/OpenWeatherMap/commit/12bae642085d40aeaf788cd99ab6fe89dd2118e9))

### [3.4.151](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.150...v3.4.151) (2021-01-08)


### Documentations

* update with Weather and UV-Index for January 07, 2021 ([543bcce](https://github.com/ALT-F1/OpenWeatherMap/commit/543bcce50cf20b995a58aa1d7780ba7bb4b4423a))

### [3.4.150](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.149...v3.4.150) (2021-01-07)


### Documentations

* update with Weather and UV-Index for January 06, 2021 ([3540549](https://github.com/ALT-F1/OpenWeatherMap/commit/354054983a8585709c699ecfb7d31b4d4840ee6d))

### [3.4.149](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.148...v3.4.149) (2021-01-06)


### Documentations

* update with Weather and UV-Index for January 05, 2021 ([b0e77ab](https://github.com/ALT-F1/OpenWeatherMap/commit/b0e77abf91dc1377455a20675349f6fea36e6752))

### [3.4.148](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.147...v3.4.148) (2021-01-05)


### Documentations

* update with Weather and UV-Index for January 04, 2021 ([4277fda](https://github.com/ALT-F1/OpenWeatherMap/commit/4277fda51c6afa3833dd1da731ed31f465af9dd2))

### [3.4.147](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.146...v3.4.147) (2021-01-04)


### Documentations

* update with Weather and UV-Index for January 03, 2021 ([90c3d3d](https://github.com/ALT-F1/OpenWeatherMap/commit/90c3d3d259bdcc013477a608b34ede29dc9a13fb))

### [3.4.146](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.145...v3.4.146) (2021-01-03)


### Documentations

* update with Weather and UV-Index for January 02, 2021 ([27967b4](https://github.com/ALT-F1/OpenWeatherMap/commit/27967b4d1c2a7e010b31ee259f5b7a4d8423efdc))

### [3.4.145](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.144...v3.4.145) (2021-01-02)


### Documentations

* update with Weather and UV-Index for January 01, 2021 ([41bf052](https://github.com/ALT-F1/OpenWeatherMap/commit/41bf052abf4beff8ce68300a438c3e483355ac7a))

### [3.4.144](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.143...v3.4.144) (2021-01-01)


### Documentations

* update with Weather and UV-Index for December 31, 2020 ([b2e18eb](https://github.com/ALT-F1/OpenWeatherMap/commit/b2e18ebd30230dd1570ad3773e31661135d53d3d))

### [3.4.143](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.142...v3.4.143) (2020-12-31)


### Documentations

* update with Weather and UV-Index for December 30, 2020 ([9c79fdc](https://github.com/ALT-F1/OpenWeatherMap/commit/9c79fdcf933aa174be472f4c336a63c58580cd83))

### [3.4.142](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.141...v3.4.142) (2020-12-30)


### Documentations

* update with Weather and UV-Index for December 29, 2020 ([be64e50](https://github.com/ALT-F1/OpenWeatherMap/commit/be64e5072ed90b866305f4be2d623fd718423eca))

### [3.4.141](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.140...v3.4.141) (2020-12-29)


### Documentations

* update with Weather and UV-Index for December 28, 2020 ([65fba7e](https://github.com/ALT-F1/OpenWeatherMap/commit/65fba7e789dcc15d50deb0f54d02b2fcddee60ce))

### [3.4.140](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.139...v3.4.140) (2020-12-28)


### Documentations

* update with Weather and UV-Index for December 27, 2020 ([5072f5d](https://github.com/ALT-F1/OpenWeatherMap/commit/5072f5dbf492ddab7cac1ee885e0de6bc7806025))

### [3.4.139](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.138...v3.4.139) (2020-12-26)


### Documentations

* update with Weather and UV-Index for December 25, 2020 ([3ef6b57](https://github.com/ALT-F1/OpenWeatherMap/commit/3ef6b5752bfaddfb9f748856d04706297602bbdf))

### [3.4.138](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.137...v3.4.138) (2020-12-25)


### Documentations

* update with Weather and UV-Index for December 24, 2020 ([2352a9b](https://github.com/ALT-F1/OpenWeatherMap/commit/2352a9ba8767f1a7b896cf67ea4dd5d6af143b3a))

### [3.4.137](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.136...v3.4.137) (2020-12-24)


### Documentations

* update with Weather and UV-Index for December 23, 2020 ([815bebb](https://github.com/ALT-F1/OpenWeatherMap/commit/815bebb8f2b7aa84bdebaea517b687f7b54149f0))

### [3.4.136](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.135...v3.4.136) (2020-12-23)


### Documentations

* update with Weather and UV-Index for December 22, 2020 ([c5f21d0](https://github.com/ALT-F1/OpenWeatherMap/commit/c5f21d08c1ecab4ddade0cc17419d790eec6d4f9))

### [3.4.135](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.134...v3.4.135) (2020-12-22)


### Documentations

* update with Weather and UV-Index for December 21, 2020 ([3e34322](https://github.com/ALT-F1/OpenWeatherMap/commit/3e34322125c2a7ac05fec2e52dc580904021bd70))

### [3.4.134](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.133...v3.4.134) (2020-12-21)


### Documentations

* update with Weather and UV-Index for December 20, 2020 ([8da2f5a](https://github.com/ALT-F1/OpenWeatherMap/commit/8da2f5a8499cee4384439d0f4c7c6eacf7b47ecb))

### [3.4.133](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.132...v3.4.133) (2020-12-20)


### Documentations

* update with Weather and UV-Index for December 19, 2020 ([c17017e](https://github.com/ALT-F1/OpenWeatherMap/commit/c17017e0d8237ce208efb4a65be3b9295220a5c5))

### [3.4.132](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.131...v3.4.132) (2020-12-18)


### Documentations

* update with Weather and UV-Index for December 17, 2020 ([0f73804](https://github.com/ALT-F1/OpenWeatherMap/commit/0f7380486de17d0195c379e14ad271a5fb4fe5eb))

### [3.4.131](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.130...v3.4.131) (2020-12-17)


### Documentations

* update with Weather and UV-Index for December 16, 2020 ([e769b1f](https://github.com/ALT-F1/OpenWeatherMap/commit/e769b1f933c8a77c68238cc7917ddb2208539606))

### [3.4.130](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.129...v3.4.130) (2020-12-16)


### Documentations

* update with Weather and UV-Index for December 15, 2020 ([76dc91c](https://github.com/ALT-F1/OpenWeatherMap/commit/76dc91ceceb670feffb30e5fed2addfac9588627))

### [3.4.129](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.128...v3.4.129) (2020-12-15)


### Documentations

* update with Weather and UV-Index for December 14, 2020 ([28813c8](https://github.com/ALT-F1/OpenWeatherMap/commit/28813c8a53933b5656257360ef63417dfc535a35))

### [3.4.128](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.127...v3.4.128) (2020-12-14)


### Documentations

* update with Weather and UV-Index for December 13, 2020 ([7423592](https://github.com/ALT-F1/OpenWeatherMap/commit/742359260a75594610e1c5ad459d3db516248aad))

### [3.4.127](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.126...v3.4.127) (2020-12-13)


### Documentations

* update with Weather and UV-Index for December 12, 2020 ([9296d11](https://github.com/ALT-F1/OpenWeatherMap/commit/9296d11da06fac2657c6b07a0465aa1deb33ba13))

### [3.4.126](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.125...v3.4.126) (2020-12-10)


### Documentations

* update with Weather and UV-Index for December 09, 2020 ([b060386](https://github.com/ALT-F1/OpenWeatherMap/commit/b0603866be02261fbca0554e9ee55e0930977649))

### [3.4.125](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.124...v3.4.125) (2020-12-09)


### Documentations

* update with Weather and UV-Index for December 08, 2020 ([02ac601](https://github.com/ALT-F1/OpenWeatherMap/commit/02ac6016a65483677eaf1f328ec4c086a5df0e0c))

### [3.4.124](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.123...v3.4.124) (2020-12-08)


### Documentations

* update with Weather and UV-Index for December 07, 2020 ([7730c8b](https://github.com/ALT-F1/OpenWeatherMap/commit/7730c8bc1bcd3493fe60ca9adb71ebb7cea62e5c))

### [3.4.123](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.122...v3.4.123) (2020-12-07)


### Documentations

* update with Weather and UV-Index for December 06, 2020 ([b0d6acd](https://github.com/ALT-F1/OpenWeatherMap/commit/b0d6acd3d8d83dec9903e09b6ab211eee281d884))

### [3.4.122](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.121...v3.4.122) (2020-12-06)


### Documentations

* update with Weather and UV-Index for December 05, 2020 ([44903d4](https://github.com/ALT-F1/OpenWeatherMap/commit/44903d479c5e07fee574e94a9856c45aa1bbe67f))

### [3.4.121](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.120...v3.4.121) (2020-12-05)


### Documentations

* update with Weather and UV-Index for December 04, 2020 ([e2d5cb3](https://github.com/ALT-F1/OpenWeatherMap/commit/e2d5cb3db381f355b57aaf7e09d9f2a0f9350ad2))

### [3.4.120](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.119...v3.4.120) (2020-12-04)


### Documentations

* update with Weather and UV-Index for December 03, 2020 ([cbab3fd](https://github.com/ALT-F1/OpenWeatherMap/commit/cbab3fd10a4aa2401b90df6bc794aa7cb7b80e61))

### [3.4.119](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.118...v3.4.119) (2020-12-03)


### Documentations

* update with Weather and UV-Index for December 02, 2020 ([2d6762f](https://github.com/ALT-F1/OpenWeatherMap/commit/2d6762fda3e694bad4b83e8a3d5cd4d165d240d5))

### [3.4.118](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.117...v3.4.118) (2020-12-02)


### Documentations

* update with Weather and UV-Index for December 01, 2020 ([5712852](https://github.com/ALT-F1/OpenWeatherMap/commit/5712852c82ee73ebb1163cb55c3851547bdc3ad2))

### [3.4.117](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.116...v3.4.117) (2020-12-01)


### Documentations

* update with Weather and UV-Index for November 30, 2020 ([499ffe5](https://github.com/ALT-F1/OpenWeatherMap/commit/499ffe5cb330bd51614562cb8b1c70229164c0dc))

### [3.4.116](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.115...v3.4.116) (2020-11-30)


### Documentations

* update with Weather and UV-Index for November 29, 2020 ([d31be0d](https://github.com/ALT-F1/OpenWeatherMap/commit/d31be0d23d39b6933164310996e49b566f6699a3))

### [3.4.115](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.114...v3.4.115) (2020-11-29)


### Documentations

* update with Weather and UV-Index for November 28, 2020 ([270f425](https://github.com/ALT-F1/OpenWeatherMap/commit/270f425482b50523443a762a38d6386d187a0a3b))

### [3.4.114](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.113...v3.4.114) (2020-11-28)


### Documentations

* update with Weather and UV-Index for November 27, 2020 ([bcfb67b](https://github.com/ALT-F1/OpenWeatherMap/commit/bcfb67bc7c2c2d00d574a612b0fe2d79c5ce5427))

### [3.4.113](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.112...v3.4.113) (2020-11-27)


### Documentations

* update with Weather and UV-Index for November 26, 2020 ([052a242](https://github.com/ALT-F1/OpenWeatherMap/commit/052a2421dd70db3047a5d288fccaff04d2a5530c))

### [3.4.112](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.111...v3.4.112) (2020-11-26)


### Documentations

* update with Weather and UV-Index for November 25, 2020 ([6000d6c](https://github.com/ALT-F1/OpenWeatherMap/commit/6000d6c96cabb92df50a6a99fd3416717acff3b3))

### [3.4.111](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.110...v3.4.111) (2020-11-25)


### Documentations

* update with Weather and UV-Index for November 24, 2020 ([4cf685e](https://github.com/ALT-F1/OpenWeatherMap/commit/4cf685e1822c98623e34c74044841f492dfb7017))

### [3.4.110](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.109...v3.4.110) (2020-11-24)


### Documentations

* update with Weather and UV-Index for November 23, 2020 ([40d1b18](https://github.com/ALT-F1/OpenWeatherMap/commit/40d1b1840bb3c2340f0ed2c0175301ccbd5dd8e5))

### [3.4.109](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.108...v3.4.109) (2020-11-23)


### Documentations

* update with Weather and UV-Index for November 22, 2020 ([edfbcc4](https://github.com/ALT-F1/OpenWeatherMap/commit/edfbcc41f88917e0fb8c19e7d02954686d441114))

### [3.4.108](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.107...v3.4.108) (2020-11-22)


### Documentations

* update with Weather and UV-Index for November 21, 2020 ([e6fb8e9](https://github.com/ALT-F1/OpenWeatherMap/commit/e6fb8e972971b946f1d0580dfb11a41533ecc335))

### [3.4.107](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.106...v3.4.107) (2020-11-21)


### Documentations

* update with Weather and UV-Index for November 20, 2020 ([9c4e46a](https://github.com/ALT-F1/OpenWeatherMap/commit/9c4e46acff7d61c67396763066cbec0d20f59343))

### [3.4.106](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.105...v3.4.106) (2020-11-20)


### Documentations

* update with Weather and UV-Index for November 19, 2020 ([08c42d6](https://github.com/ALT-F1/OpenWeatherMap/commit/08c42d65f17fbf24b4d0c672d4884b9babb4ec14))

### [3.4.105](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.104...v3.4.105) (2020-11-19)


### Documentations

* update with Weather and UV-Index for November 18, 2020 ([2a0f94b](https://github.com/ALT-F1/OpenWeatherMap/commit/2a0f94b74d7549a4d4535c54af24d979de01477f))

### [3.4.104](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.103...v3.4.104) (2020-11-18)


### Documentations

* update with Weather and UV-Index for November 17, 2020 ([fe0e39e](https://github.com/ALT-F1/OpenWeatherMap/commit/fe0e39e301ee23146ffb2da997986ad763bd7037))

### [3.4.103](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.102...v3.4.103) (2020-11-17)


### Documentations

* update with Weather and UV-Index for November 16, 2020 ([54982e6](https://github.com/ALT-F1/OpenWeatherMap/commit/54982e64af8b932b6fed62a20259a1e2096cae5d))

### [3.4.102](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.101...v3.4.102) (2020-11-16)


### Documentations

* update with Weather and UV-Index for November 15, 2020 ([cc42409](https://github.com/ALT-F1/OpenWeatherMap/commit/cc42409bb788cd4caf2316dd2617ecd719ee6002))

### [3.4.101](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.100...v3.4.101) (2020-11-15)


### Documentations

* update with Weather and UV-Index for November 14, 2020 ([075d881](https://github.com/ALT-F1/OpenWeatherMap/commit/075d8818cce5c782014a02f71f0ba8f209e5cef4))

### [3.4.100](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.99...v3.4.100) (2020-11-14)


### Documentations

* update with Weather and UV-Index for November 13, 2020 ([8e4ac85](https://github.com/ALT-F1/OpenWeatherMap/commit/8e4ac8566167e07b72b08e79fecf91472b84e49a))

### [3.4.99](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.98...v3.4.99) (2020-11-13)


### Documentations

* update with Weather and UV-Index for November 12, 2020 ([29021bc](https://github.com/ALT-F1/OpenWeatherMap/commit/29021bc8e1d44b7873dd87a1592159645daacee1))

### [3.4.98](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.97...v3.4.98) (2020-11-12)


### Documentations

* update with Weather and UV-Index for November 11, 2020 ([d80f751](https://github.com/ALT-F1/OpenWeatherMap/commit/d80f7519c2c6335cb0967ddb25d9a50cc170f4e8))

### [3.4.97](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.96...v3.4.97) (2020-11-11)


### Documentations

* update with Weather and UV-Index for November 10, 2020 ([456c3df](https://github.com/ALT-F1/OpenWeatherMap/commit/456c3dfe3c756df6453642defa98ae6e4959e6e3))

### [3.4.96](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.95...v3.4.96) (2020-11-10)


### Documentations

* update with Weather and UV-Index for November 09, 2020 ([bfd7955](https://github.com/ALT-F1/OpenWeatherMap/commit/bfd795580bf3aa806a7797d83a0b48c9023c845c))

### [3.4.95](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.94...v3.4.95) (2020-11-09)


### Documentations

* update with Weather and UV-Index for November 08, 2020 ([496abdb](https://github.com/ALT-F1/OpenWeatherMap/commit/496abdbd0c64a21099da084d11bd843d8e6b29b0))

### [3.4.94](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.93...v3.4.94) (2020-11-08)


### Documentations

* update with Weather and UV-Index for November 07, 2020 ([20e1428](https://github.com/ALT-F1/OpenWeatherMap/commit/20e14282129cdc351575f6edd7aeb9c7f28081d1))

### [3.4.93](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.92...v3.4.93) (2020-11-07)


### Documentations

* update with Weather and UV-Index for November 06, 2020 ([bf0dc34](https://github.com/ALT-F1/OpenWeatherMap/commit/bf0dc340bc1d1c4ad82f0bc89f1a4428bae21ba6))

### [3.4.92](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.91...v3.4.92) (2020-11-06)


### Documentations

* update with Weather and UV-Index for November 05, 2020 ([3c671b2](https://github.com/ALT-F1/OpenWeatherMap/commit/3c671b2b9440ad74922fe9ff9cc4a036bd8eab49))

### [3.4.91](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.90...v3.4.91) (2020-11-05)


### Documentations

* update with Weather and UV-Index for November 04, 2020 ([9bf201a](https://github.com/ALT-F1/OpenWeatherMap/commit/9bf201a7e3ece5189aa6c3c2a73e63353aa3e855))

### [3.4.90](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.89...v3.4.90) (2020-11-04)


### Documentations

* update with Weather and UV-Index for November 03, 2020 ([8dd2548](https://github.com/ALT-F1/OpenWeatherMap/commit/8dd2548c3aeac596234415c2b3490ea18909d70e))

### [3.4.89](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.88...v3.4.89) (2020-11-03)


### Documentations

* update with Weather and UV-Index for November 02, 2020 ([b0156da](https://github.com/ALT-F1/OpenWeatherMap/commit/b0156da8006ebc4af004ed08d829db489b21ece7))

### [3.4.88](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.87...v3.4.88) (2020-11-01)


### Documentations

* update with Weather and UV-Index for October 31, 2020 ([4f4c6fd](https://github.com/ALT-F1/OpenWeatherMap/commit/4f4c6fdbf3552225897278f05cc8fea89c972a56))

### [3.4.87](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.86...v3.4.87) (2020-10-31)


### Documentations

* update with Weather and UV-Index for October 30, 2020 ([cce5a9e](https://github.com/ALT-F1/OpenWeatherMap/commit/cce5a9e9eaa5504ce9ce29991d807598239fc591))

### [3.4.86](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.85...v3.4.86) (2020-10-30)


### Documentations

* update with Weather and UV-Index for October 29, 2020 ([d7bfd68](https://github.com/ALT-F1/OpenWeatherMap/commit/d7bfd68dc1e8c107e3f537d837d05fb90f17b6e4))

### [3.4.85](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.84...v3.4.85) (2020-10-29)


### Documentations

* update with Weather and UV-Index for October 28, 2020 ([da1046a](https://github.com/ALT-F1/OpenWeatherMap/commit/da1046a5266a1a0b4996feb9c2bc3a1d717b45d2))

### [3.4.84](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.83...v3.4.84) (2020-10-28)


### Documentations

* update with Weather and UV-Index for October 27, 2020 ([fbaac82](https://github.com/ALT-F1/OpenWeatherMap/commit/fbaac829059a90c0d0ffa86c809b55bdbd0f388b))

### [3.4.83](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.82...v3.4.83) (2020-10-27)


### Documentations

* update with Weather and UV-Index for October 26, 2020 ([7c72ed5](https://github.com/ALT-F1/OpenWeatherMap/commit/7c72ed55c466f996a2ca182e60f8fbdec49a4f86))

### [3.4.82](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.81...v3.4.82) (2020-10-26)


### Documentations

* update with Weather and UV-Index for October 25, 2020 ([9bb6438](https://github.com/ALT-F1/OpenWeatherMap/commit/9bb64381e64728824efdf8b743c859c6ae105df4))

### [3.4.81](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.80...v3.4.81) (2020-10-25)


### Documentations

* update with Weather and UV-Index for October 24, 2020 ([d020483](https://github.com/ALT-F1/OpenWeatherMap/commit/d0204838f057855ee26ec5bfa0820ef40a311433))

### [3.4.80](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.79...v3.4.80) (2020-10-24)


### Documentations

* update with Weather and UV-Index for October 23, 2020 ([5747e1d](https://github.com/ALT-F1/OpenWeatherMap/commit/5747e1de3111d10f9605c3e519d8c73c584a92c4))

### [3.4.79](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.78...v3.4.79) (2020-10-23)


### Documentations

* update with Weather and UV-Index for October 22, 2020 ([18d4844](https://github.com/ALT-F1/OpenWeatherMap/commit/18d4844d988663a00323a45ce58d503b01a5769c))

### [3.4.78](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.77...v3.4.78) (2020-10-22)


### Documentations

* update with Weather and UV-Index for October 21, 2020 ([0c8b4b9](https://github.com/ALT-F1/OpenWeatherMap/commit/0c8b4b9d0d9f3d329a27263edf83b8e13517ab61))

### [3.4.77](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.76...v3.4.77) (2020-10-21)


### Documentations

* update with Weather and UV-Index for October 20, 2020 ([26b5f9c](https://github.com/ALT-F1/OpenWeatherMap/commit/26b5f9ca15a6f6f9b684dc37eac6109ff925fb21))

### [3.4.76](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.75...v3.4.76) (2020-10-20)


### Documentations

* update with Weather and UV-Index for October 19, 2020 ([a1659a2](https://github.com/ALT-F1/OpenWeatherMap/commit/a1659a26cbacab347db07411bf65e36822081e50))

### [3.4.75](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.74...v3.4.75) (2020-10-19)


### Documentations

* update with Weather and UV-Index for October 18, 2020 ([a57ca58](https://github.com/ALT-F1/OpenWeatherMap/commit/a57ca5814e12780fbc9564dad3b6c0cebfaecca5))

### [3.4.74](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.73...v3.4.74) (2020-10-19)

### [3.4.73](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.72...v3.4.73) (2020-10-18)


### Documentations

* update with Weather and UV-Index for October 17, 2020 ([5d7843a](https://github.com/ALT-F1/OpenWeatherMap/commit/5d7843a3df2f24b3d80c3fa27d09424f12a35b46))

### [3.4.72](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.71...v3.4.72) (2020-10-17)


### Documentations

* update with Weather and UV-Index for October 16, 2020 ([9331ad8](https://github.com/ALT-F1/OpenWeatherMap/commit/9331ad8e954bc584f10bd95df5113279725284d7))

### [3.4.71](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.70...v3.4.71) (2020-10-16)


### Documentations

* update with Weather and UV-Index for October 15, 2020 ([f0509ca](https://github.com/ALT-F1/OpenWeatherMap/commit/f0509ca25966d4400d7fa82698553778d5e3c134))

### [3.4.70](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.69...v3.4.70) (2020-10-15)


### Documentations

* update with Weather and UV-Index for October 14, 2020 ([dcfa636](https://github.com/ALT-F1/OpenWeatherMap/commit/dcfa636bc3ae0424b0d69ca9e898fcb2dbd9920c))

### [3.4.69](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.68...v3.4.69) (2020-10-14)


### Documentations

* update with Weather and UV-Index for October 13, 2020 ([0bae8fa](https://github.com/ALT-F1/OpenWeatherMap/commit/0bae8facfd8f9db1b10a24aae63ba5f3552693fe))

### [3.4.68](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.67...v3.4.68) (2020-10-13)


### Documentations

* update with Weather and UV-Index for October 12, 2020 ([faf24f2](https://github.com/ALT-F1/OpenWeatherMap/commit/faf24f2ca09fb1d8356dd128a3fd947acf4fc469))

### [3.4.67](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.66...v3.4.67) (2020-10-12)


### Documentations

* update with Weather and UV-Index for October 11, 2020 ([16d861e](https://github.com/ALT-F1/OpenWeatherMap/commit/16d861ecb705b65eb9fbdbcf28b6692f020ee723))

### [3.4.66](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.65...v3.4.66) (2020-10-11)


### Documentations

* update with Weather and UV-Index for October 10, 2020 ([4413542](https://github.com/ALT-F1/OpenWeatherMap/commit/4413542ee7426344bc8a95cb8cb541da7b9405b6))

### [3.4.65](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.64...v3.4.65) (2020-10-10)


### Documentations

* update with Weather and UV-Index for October 09, 2020 ([667af4c](https://github.com/ALT-F1/OpenWeatherMap/commit/667af4cbbb30c2344f91f9326153bdfb06f7c127))

### [3.4.64](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.63...v3.4.64) (2020-10-09)


### Documentations

* update with Weather and UV-Index for October 08, 2020 ([a2a58d9](https://github.com/ALT-F1/OpenWeatherMap/commit/a2a58d91a0bf5e3f727530b1da571b695ab40c6d))

### [3.4.63](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.62...v3.4.63) (2020-10-08)


### Documentations

* update with Weather and UV-Index for October 07, 2020 ([0338259](https://github.com/ALT-F1/OpenWeatherMap/commit/03382596a829a0e4e983d28ab3b33b8d167c4676))

### [3.4.62](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.61...v3.4.62) (2020-10-07)


### Documentations

* update with Weather and UV-Index for October 06, 2020 ([8d4c755](https://github.com/ALT-F1/OpenWeatherMap/commit/8d4c7551f08456078cd8beeb36f9f57b45e07e9f))

### [3.4.61](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.60...v3.4.61) (2020-10-06)


### Documentations

* update with Weather and UV-Index for October 05, 2020 ([38aaf32](https://github.com/ALT-F1/OpenWeatherMap/commit/38aaf32717d7ae7bc54bfe5b47675cca028bfb98))

### [3.4.60](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.59...v3.4.60) (2020-10-05)


### Documentations

* update with Weather and UV-Index for October 04, 2020 ([9c1baae](https://github.com/ALT-F1/OpenWeatherMap/commit/9c1baaee1ea5402e4a9e226994dda04d88993e6d))

### [3.4.59](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.58...v3.4.59) (2020-10-04)


### Documentations

* update with Weather and UV-Index for October 03, 2020 ([9f7bb6a](https://github.com/ALT-F1/OpenWeatherMap/commit/9f7bb6a933981a90875fcb268de639a165fd1f71))

### [3.4.58](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.57...v3.4.58) (2020-10-02)


### Documentations

* update with Weather and UV-Index for October 01, 2020 ([59c0796](https://github.com/ALT-F1/OpenWeatherMap/commit/59c07967234aa5ddb189b2724a73b377ecee4a33))

### [3.4.57](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.56...v3.4.57) (2020-10-01)


### Documentations

* update with Weather and UV-Index for September 30, 2020 ([10848f6](https://github.com/ALT-F1/OpenWeatherMap/commit/10848f6f1b8ac0db2381c1368613fe4c83c2baf3))

### [3.4.56](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.55...v3.4.56) (2020-09-30)


### Documentations

* update with Weather and UV-Index for September 29, 2020 ([d0a841a](https://github.com/ALT-F1/OpenWeatherMap/commit/d0a841a43a56be831e3362c1735b3f245e38611d))

### [3.4.55](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.54...v3.4.55) (2020-09-29)


### Documentations

* update with Weather and UV-Index for September 28, 2020 ([606e75d](https://github.com/ALT-F1/OpenWeatherMap/commit/606e75d5e49a137ddfb176ee71ec5809f13c82d3))

### [3.4.54](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.53...v3.4.54) (2020-09-28)


### Documentations

* update with Weather and UV-Index for September 27, 2020 ([07e1e5a](https://github.com/ALT-F1/OpenWeatherMap/commit/07e1e5a05fcdcd514f36c4519f2eeb6053b80fec))

### [3.4.53](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.52...v3.4.53) (2020-09-27)


### Documentations

* update with Weather and UV-Index for September 26, 2020 ([20f480d](https://github.com/ALT-F1/OpenWeatherMap/commit/20f480df31a67963da3755375b4d4631936a4c44))

### [3.4.52](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.51...v3.4.52) (2020-09-26)


### Documentations

* update with Weather and UV-Index for September 25, 2020 ([0835f5b](https://github.com/ALT-F1/OpenWeatherMap/commit/0835f5b9222ddb6df1f3debc9ae0eddb31e9fb6d))

### [3.4.51](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.50...v3.4.51) (2020-09-25)


### Documentations

* update with Weather and UV-Index for September 24, 2020 ([88f1c72](https://github.com/ALT-F1/OpenWeatherMap/commit/88f1c72c51884392b8b6409ada802d5a60ee0398))

### [3.4.50](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.49...v3.4.50) (2020-09-24)


### Documentations

* update with Weather and UV-Index for September 23, 2020 ([bd80f5b](https://github.com/ALT-F1/OpenWeatherMap/commit/bd80f5b5270d82c9ae9ad43864293cb8be2f7691))

### [3.4.49](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.48...v3.4.49) (2020-09-23)


### Documentations

* update with Weather and UV-Index for September 22, 2020 ([0da3c25](https://github.com/ALT-F1/OpenWeatherMap/commit/0da3c253234823a39cdde060cee1ec27187d8ded))

### [3.4.48](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.47...v3.4.48) (2020-09-22)


### Documentations

* update with Weather and UV-Index for September 21, 2020 ([71fecc8](https://github.com/ALT-F1/OpenWeatherMap/commit/71fecc8c90eecda46cbfbe6ce567fb72d19f1f50))

### [3.4.47](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.46...v3.4.47) (2020-09-21)


### Documentations

* update with Weather and UV-Index for September 20, 2020 ([3e2a765](https://github.com/ALT-F1/OpenWeatherMap/commit/3e2a7652a9a4dfeecbb4e097a02a3caceff9bf61))

### [3.4.46](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.45...v3.4.46) (2020-09-19)


### Documentations

* update with Weather and UV-Index for September 18, 2020 ([ae83a66](https://github.com/ALT-F1/OpenWeatherMap/commit/ae83a66b77e5728f88e49b2116f41ced01ae7ff5))

### [3.4.45](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.44...v3.4.45) (2020-09-18)


### Documentations

* update with Weather and UV-Index for September 17, 2020 ([c47230a](https://github.com/ALT-F1/OpenWeatherMap/commit/c47230a54a480733b5ebeca24f1a9c57772cdfee))

### [3.4.44](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.43...v3.4.44) (2020-09-17)


### Documentations

* update with Weather and UV-Index for September 16, 2020 ([477c719](https://github.com/ALT-F1/OpenWeatherMap/commit/477c719a8f0bd207156f9437b6d4935ca68b6449))

### [3.4.43](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.42...v3.4.43) (2020-09-16)


### Documentations

* update with Weather and UV-Index for September 15, 2020 ([da7f865](https://github.com/ALT-F1/OpenWeatherMap/commit/da7f865031354818afe7688a04adaffd309df8c1))

### [3.4.42](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.41...v3.4.42) (2020-09-15)


### Documentations

* update with Weather and UV-Index for September 14, 2020 ([f3158f7](https://github.com/ALT-F1/OpenWeatherMap/commit/f3158f7a81a83b8b8292680d3cd527ed4f2cfc07))

### [3.4.41](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.40...v3.4.41) (2020-09-14)


### Documentations

* update with Weather and UV-Index for September 13, 2020 ([4c70f72](https://github.com/ALT-F1/OpenWeatherMap/commit/4c70f72ba0a6e25f907390ca024427d7fd6b7bbd))

### [3.4.40](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.39...v3.4.40) (2020-09-12)


### Documentations

* update with Weather and UV-Index for September 11, 2020 ([523af9a](https://github.com/ALT-F1/OpenWeatherMap/commit/523af9a944199f6e2a915f18387ff7603e731d96))

### [3.4.39](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.38...v3.4.39) (2020-09-11)


### Documentations

* update with Weather and UV-Index for September 10, 2020 ([e147acb](https://github.com/ALT-F1/OpenWeatherMap/commit/e147acb23b8842a6a5cae03871ab53a371c93efa))

### [3.4.38](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.37...v3.4.38) (2020-09-10)


### Documentations

* update with Weather and UV-Index for September 09, 2020 ([813ebd9](https://github.com/ALT-F1/OpenWeatherMap/commit/813ebd9c792f2c68943fa963e0e71a1363564b9c))

### [3.4.37](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.36...v3.4.37) (2020-09-09)


### Documentations

* update with Weather and UV-Index for September 08, 2020 ([e305f51](https://github.com/ALT-F1/OpenWeatherMap/commit/e305f512009387927e2dbc5dbcb7159df4886769))

### [3.4.36](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.35...v3.4.36) (2020-09-08)


### Documentations

* update with Weather and UV-Index for September 07, 2020 ([9481e86](https://github.com/ALT-F1/OpenWeatherMap/commit/9481e86e62f02189be85a9f8dad59e85178e8f3b))

### [3.4.35](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.34...v3.4.35) (2020-09-07)


### Documentations

* update with Weather and UV-Index for September 06, 2020 ([c55972f](https://github.com/ALT-F1/OpenWeatherMap/commit/c55972f27d9b7bf84507591716658bfd982d3125))

### [3.4.34](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.33...v3.4.34) (2020-09-06)


### Documentations

* update with Weather and UV-Index for September 05, 2020 ([421135e](https://github.com/ALT-F1/OpenWeatherMap/commit/421135ecea8cd9a11a02b22e304b7ee5956ffb40))

### [3.4.33](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.32...v3.4.33) (2020-09-04)


### Documentations

* update with Weather and UV-Index for September 03, 2020 ([20ba189](https://github.com/ALT-F1/OpenWeatherMap/commit/20ba189bc55febfeda72a6c7a87d41ffe9d1c52b))

### [3.4.32](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.31...v3.4.32) (2020-09-03)


### Documentations

* update with Weather and UV-Index for September 02, 2020 ([144a8b2](https://github.com/ALT-F1/OpenWeatherMap/commit/144a8b22456239f8ea489dcf07f77ec8470a2e35))

### [3.4.31](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.30...v3.4.31) (2020-09-02)


### Documentations

* update with Weather and UV-Index for September 01, 2020 ([82ceb11](https://github.com/ALT-F1/OpenWeatherMap/commit/82ceb11e14c38fe50b9fd94606404f5dec39a285))

### [3.4.30](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.29...v3.4.30) (2020-08-31)


### Documentations

* update with Weather and UV-Index for August 30, 2020 ([b792e0f](https://github.com/ALT-F1/OpenWeatherMap/commit/b792e0fd80a64b8103b2420e7d2a970224e716cd))

### [3.4.29](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.28...v3.4.29) (2020-08-30)


### Documentations

* update with Weather and UV-Index for August 29, 2020 ([431776d](https://github.com/ALT-F1/OpenWeatherMap/commit/431776df3e7445a1a5498858f2adf101d719dbcf))

### [3.4.28](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.27...v3.4.28) (2020-08-29)


### Documentations

* update with Weather and UV-Index for August 28, 2020 ([cd367a0](https://github.com/ALT-F1/OpenWeatherMap/commit/cd367a04a9a2f5755f9a36f950c6bb3e46c06f9c))

### [3.4.27](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.26...v3.4.27) (2020-08-28)


### Documentations

* update with Weather and UV-Index for August 27, 2020 ([63c0a85](https://github.com/ALT-F1/OpenWeatherMap/commit/63c0a85dc9c463bc1f4d860367ca2f20378a4a73))

### [3.4.26](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.25...v3.4.26) (2020-08-27)


### Documentations

* update with Weather and UV-Index for August 26, 2020 ([57dbd77](https://github.com/ALT-F1/OpenWeatherMap/commit/57dbd7768022ca576606bb60f019e1769c798ce2))

### [3.4.25](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.24...v3.4.25) (2020-08-26)


### Documentations

* update with Weather and UV-Index for August 25, 2020 ([01a6613](https://github.com/ALT-F1/OpenWeatherMap/commit/01a6613128e7fb8a1aa45af915d2916b2b52b98c))

### [3.4.24](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.23...v3.4.24) (2020-08-25)


### Documentations

* update with Weather and UV-Index for August 24, 2020 ([d8ab2ed](https://github.com/ALT-F1/OpenWeatherMap/commit/d8ab2edd050f659123aa01fabd51833b21eae3d9))

### [3.4.23](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.22...v3.4.23) (2020-08-24)


### Documentations

* update with Weather and UV-Index for August 23, 2020 ([8d77ee3](https://github.com/ALT-F1/OpenWeatherMap/commit/8d77ee334fe45b3e2beaa3f8389cf32f0d38ad1d))

### [3.4.22](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.21...v3.4.22) (2020-08-23)


### Documentations

* update with Weather and UV-Index for August 22, 2020 ([06a2730](https://github.com/ALT-F1/OpenWeatherMap/commit/06a2730f922771ea472feb3d879c0b7df7c056f3))

### [3.4.21](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.20...v3.4.21) (2020-08-21)


### Documentations

* update with Weather and UV-Index for August 20, 2020 ([e2d0e0c](https://github.com/ALT-F1/OpenWeatherMap/commit/e2d0e0c884d06a10262386839b9e6b4f9b15ac40))

### [3.4.20](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.19...v3.4.20) (2020-08-20)


### Documentations

* update with Weather and UV-Index for August 19, 2020 ([1c10f1d](https://github.com/ALT-F1/OpenWeatherMap/commit/1c10f1d2dc3bbf897ab09778245bb191a902b2d6))

### [3.4.19](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.18...v3.4.19) (2020-08-19)


### Documentations

* update with Weather and UV-Index for August 18, 2020 ([283d5c6](https://github.com/ALT-F1/OpenWeatherMap/commit/283d5c6bad5f649d547e732fff1993b881f1f3a9))

### [3.4.18](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.17...v3.4.18) (2020-08-18)


### Documentations

* update with Weather and UV-Index for August 17, 2020 ([f3b8baa](https://github.com/ALT-F1/OpenWeatherMap/commit/f3b8baa7d01f82e9e67a3657f640c2966d91a979))

### [3.4.17](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.16...v3.4.17) (2020-08-17)


### Documentations

* update with Weather and UV-Index for August 16, 2020 ([8b66ec2](https://github.com/ALT-F1/OpenWeatherMap/commit/8b66ec2c7e7d279bc4779da84c7038a72d259cf9))

### [3.4.16](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.15...v3.4.16) (2020-08-16)


### Documentations

* update with Weather and UV-Index for August 15, 2020 ([e865127](https://github.com/ALT-F1/OpenWeatherMap/commit/e865127e10b69b2c42c9b415fdb703851782cf6b))

### [3.4.15](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.14...v3.4.15) (2020-08-15)


### Documentations

* update with Weather and UV-Index for August 14, 2020 ([cf60dd6](https://github.com/ALT-F1/OpenWeatherMap/commit/cf60dd64dfc78a9307fdd2b8fddde3eee6bbb671))

### [3.4.14](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.13...v3.4.14) (2020-08-14)


### Documentations

* update with Weather and UV-Index for August 13, 2020 ([5ae8ea5](https://github.com/ALT-F1/OpenWeatherMap/commit/5ae8ea5e3e988b1e22b2234109aa32cf1158bd2f))

### [3.4.13](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.12...v3.4.13) (2020-08-13)


### Documentations

* update with Weather and UV-Index for August 12, 2020 ([4f6118e](https://github.com/ALT-F1/OpenWeatherMap/commit/4f6118eda2506805e10f5682dfe342cea8f3c30b))

### [3.4.12](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.11...v3.4.12) (2020-08-12)


### Bug Fixes

* move secrets environment variables under an untracked directory ([bfee67b](https://github.com/ALT-F1/OpenWeatherMap/commit/bfee67bdb7985bae32e1e3caec2d2239795d97b1))

### [3.4.11](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.10...v3.4.11) (2020-08-12)


### Documentations

* update with Weather and UV-Index for August 11, 2020 ([ae859d2](https://github.com/ALT-F1/OpenWeatherMap/commit/ae859d2931d42e838cf6e8c444326b0629cdc054))

### [3.4.10](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.9...v3.4.10) (2020-08-11)


### Features

* send an email when latest data are uploaded on GitHub ([b43ac27](https://github.com/ALT-F1/OpenWeatherMap/commit/b43ac276006e07bbde961bd309000b2c516a47d1))
* send email informing when the latest data are stored on github ([f3e50e6](https://github.com/ALT-F1/OpenWeatherMap/commit/f3e50e60d1b8a546bd6a9db06912036e9ebeae7e))


### Bug Fixes

* make the display the time spent to build the datasource more easy to read ([6d8a775](https://github.com/ALT-F1/OpenWeatherMap/commit/6d8a775df2932ffc78d23bc2a56529f82b853e70))


### Chores

* add eosc.env in .gitignore ([010ed38](https://github.com/ALT-F1/OpenWeatherMap/commit/010ed38ffc37865a77526c0637b76772e659ae97))

### [3.4.9](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.8...v3.4.9) (2020-08-11)


### Documentations

* update with Weather and UV-Index for August 10, 2020 ([fab95a7](https://github.com/ALT-F1/OpenWeatherMap/commit/fab95a7c861442b6e5e6e6fae424e9fde9da6c28))

### [3.4.8](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.7...v3.4.8) (2020-08-10)


### Documentations

* update with Weather and UV-Index for August 09, 2020 ([01ba632](https://github.com/ALT-F1/OpenWeatherMap/commit/01ba632513a23e2c98c6e9ef4dd832ab1b507557))

### [3.4.7](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.6...v3.4.7) (2020-08-09)


### Documentations

* update with Weather and UV-Index for August 08, 2020 ([f0d8050](https://github.com/ALT-F1/OpenWeatherMap/commit/f0d8050f2027aec492d83b855c783a9dd0456aaf))

### [3.4.6](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.5...v3.4.6) (2020-08-08)


### Documentations

* update with Weather and UV-Index for August 07, 2020 ([ca0326b](https://github.com/ALT-F1/OpenWeatherMap/commit/ca0326b8e687f2da59c9e393a0983d9a10a10a40))

### [3.4.5](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.4...v3.4.5) (2020-08-07)


### Documentations

* update with Weather and UV-Index for August 06, 2020 ([f056884](https://github.com/ALT-F1/OpenWeatherMap/commit/f05688485783507c5a19c1562c281cb0089ae09a))

### [3.4.4](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.3...v3.4.4) (2020-08-06)


### Documentations

* update with Weather and UV-Index for August 05, 2020 ([1f1934b](https://github.com/ALT-F1/OpenWeatherMap/commit/1f1934b92fdb663943ae9d50c6af4433ee1a221d))

### [3.4.3](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.2...v3.4.3) (2020-08-05)


### Documentations

* update with Weather and UV-Index for August 04, 2020 ([621e632](https://github.com/ALT-F1/OpenWeatherMap/commit/621e632f61b88ced3a2ab87880ab60cdcbcb51d8))

### [3.4.2](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.1...v3.4.2) (2020-08-03)


### Documentations

* update with Weather and UV-Index for August 02, 2020 ([02f7ea0](https://github.com/ALT-F1/OpenWeatherMap/commit/02f7ea0205be07cab009d4bea2616b92af3a7b7d))

### [3.4.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.4.0...v3.4.1) (2020-08-02)


### Documentations

* update with Weather and UV-Index for August 01, 2020 ([93958d2](https://github.com/ALT-F1/OpenWeatherMap/commit/93958d2c92b56db0754280ff22922852557c93f2))

## [3.4.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.3.7...v3.4.0) (2020-08-02)


### Bug Fixes

* force the 'r' mode when reading a file ([25139f0](https://github.com/ALT-F1/OpenWeatherMap/commit/25139f0e2ffc14a5aad58302aace453c744da201))
* force the 'r' mode when reading a file ([91e5145](https://github.com/ALT-F1/OpenWeatherMap/commit/91e514546d47183be6a2e88b9e227ed5d29969a3))

### [3.3.7](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.3.6...v3.3.7) (2020-08-01)


### Documentations

* update with Weather and UV-Index for July 31, 2020 ([0380747](https://github.com/ALT-F1/OpenWeatherMap/commit/0380747cd0eb9c4b6b428a4314eacc90ca65b9b0))

### [3.3.6](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.3.5...v3.3.6) (2020-07-31)


### Documentations

* update with Weather and UV-Index for July 30, 2020 ([64c8e02](https://github.com/ALT-F1/OpenWeatherMap/commit/64c8e02ccb45bf0f2b5913f5b3de3b7fa78a8dc7))

### [3.3.5](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.3.4...v3.3.5) (2020-07-29)


### Documentations

* update with Weather and UV-Index for July 28, 2020 ([dde7150](https://github.com/ALT-F1/OpenWeatherMap/commit/dde7150df1b6d4a0e713be9af226e8eeda50cd80))

### [3.3.4](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.3.3...v3.3.4) (2020-07-27)


### Documentations

* update with Weather and UV-Index for July 25 and July 26 2020 ([1e0fe53](https://github.com/ALT-F1/OpenWeatherMap/commit/1e0fe535acd1abece9f2ff03ed606cb1ef1c4390))

### [3.3.3](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.3.2...v3.3.3) (2020-07-27)

### [3.3.2](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.3.1...v3.3.2) (2020-07-25)


### Documentations

* update with Weather and UV-Index for July 24, 2020 ([3003205](https://github.com/ALT-F1/OpenWeatherMap/commit/300320564367903f88be53ea6a8e6ba6ece85b42))

### [3.3.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.3.0...v3.3.1) (2020-07-24)


### Documentations

* update with Weather and UV-Index for July 23, 2020 ([06c9faf](https://github.com/ALT-F1/OpenWeatherMap/commit/06c9fafacea8d8c53580fcd91ef8c8e01a1e70ec))

## [3.3.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.22...v3.3.0) (2020-07-23)


### Features

* build .py to automatically generate the dataset and push the data on Github ([e479841](https://github.com/ALT-F1/OpenWeatherMap/commit/e4798419ec0b9564609bf362ce52d85be48987ef))

### [3.2.22](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.21...v3.2.22) (2020-07-23)


### Chores

* **release:** 3.2.19 ([b867d97](https://github.com/ALT-F1/OpenWeatherMap/commit/b867d97b3af04d7f91e97fb8e86b6f245fad6de1))


### Documentations

* update with Weather and UV-Index for July 22, 2020 ([a9ffb97](https://github.com/ALT-F1/OpenWeatherMap/commit/a9ffb97d83a827997fab7c585b085c5859e332fd))

### [3.2.21](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.20...v3.2.21) (2020-07-23)

### [3.2.20](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.19...v3.2.20) (2020-07-23)

### [3.2.19](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.18...v3.2.19) (2020-07-22)


### Documentations

* update with Weather and UV-Index for July 21, 2020 ([7eb35de](https://github.com/ALT-F1/OpenWeatherMap/commit/7eb35de21e4c5de43b163ba64664365a10b4a48d))

### [3.2.18](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.17...v3.2.18) (2020-07-21)


### Documentations

* update with Weather and UV-Index for July 20, 2020 ([abff10a](https://github.com/ALT-F1/OpenWeatherMap/commit/abff10a710c0a3c6b94cd2ed913a79197384f7dc))

### [3.2.17](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.16...v3.2.17) (2020-07-20)


### Documentations

* update with Weather and UV-Index for July 19, 2020 ([e70dfe0](https://github.com/ALT-F1/OpenWeatherMap/commit/e70dfe0c06b8bc178dbcf24624383bfe84bf9166))

### [3.2.16](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.15...v3.2.16) (2020-07-19)


### Documentations

* update with Weather and UV-Index for July 18, 2020 ([c62902d](https://github.com/ALT-F1/OpenWeatherMap/commit/c62902d3fbf50f8852b0a1f6d06803fcfd5e31e2))

### [3.2.15](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.14...v3.2.15) (2020-07-18)


### Documentations

* update with Weather and UV-Index for July 17, 2020 ([c4a3ac3](https://github.com/ALT-F1/OpenWeatherMap/commit/c4a3ac32cca81907a249d0f5b402dbb368ccb70b))

### [3.2.14](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.13...v3.2.14) (2020-07-17)


### Documentations

* update with Weather and UV-Index for July 16, 2020 ([3206eca](https://github.com/ALT-F1/OpenWeatherMap/commit/3206eca4f65f953492988f010727afd1eae144d2))

### [3.2.13](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.12...v3.2.13) (2020-07-16)


### Documentations

* update with Weather and UV-Index for July 15, 2020 ([1bc0cda](https://github.com/ALT-F1/OpenWeatherMap/commit/1bc0cda821943c2e72eddbe4f3b1e84a41fa193b))

### [3.2.12](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.11...v3.2.12) (2020-07-15)


### Documentations

* update with Weather and UV-Index for July 14, 2020 ([bc30455](https://github.com/ALT-F1/OpenWeatherMap/commit/bc304551d22e68c527b762eb02271a616e8e13d2))

### [3.2.11](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.10...v3.2.11) (2020-07-14)


### Documentations

* update with Weather and UV-Index for July 13, 2020 ([2963abf](https://github.com/ALT-F1/OpenWeatherMap/commit/2963abf3527490c083c811f0e72100659c4cae0a))

### [3.2.10](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.9...v3.2.10) (2020-07-13)


### Documentations

* update with Weather and UV-Index for July 11 and July 12, 2020 ([ec428b4](https://github.com/ALT-F1/OpenWeatherMap/commit/ec428b431dc2b04601b239a32ef96072dd822956))

### [3.2.9](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.8...v3.2.9) (2020-07-10)


### Documentations

* update with Weather and UV-Index for July 09, 2020 ([c711d01](https://github.com/ALT-F1/OpenWeatherMap/commit/c711d01faf96321d84a831a3e8343e37fbc98137))

### [3.2.8](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.7...v3.2.8) (2020-07-09)


### Documentations

* update with Weather and UV-Index for July 08, 2020 ([f81aa9f](https://github.com/ALT-F1/OpenWeatherMap/commit/f81aa9fada1ed1670cc2151a26e6f56f224b55c1))

### [3.2.7](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.6...v3.2.7) (2020-07-08)


### Documentations

* update with Weather and UV-Index for July 07, 2020 ([e60588d](https://github.com/ALT-F1/OpenWeatherMap/commit/e60588dfd6bd7f587c6c8361312ee842966a6e1f))

### [3.2.6](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.5...v3.2.6) (2020-07-07)


### Documentations

* update with Weather and UV-Index for July 06, 2020 ([c878725](https://github.com/ALT-F1/OpenWeatherMap/commit/c878725a1763eb70fdbe0b201ea23967e3833201))

### [3.2.5](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.4...v3.2.5) (2020-07-06)


### Documentations

* update with Weather and UV-Index for July 05, 2020 ([5caac9b](https://github.com/ALT-F1/OpenWeatherMap/commit/5caac9be9e237ce08446573df0e90fd8f7e410d0))

### [3.2.4](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.3...v3.2.4) (2020-07-05)


### Documentations

* update with Weather and UV-Index for July 04, 2020 ([d8ec823](https://github.com/ALT-F1/OpenWeatherMap/commit/d8ec8237c9e860b83b56d08405957c9c0776620e))

### [3.2.3](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.2...v3.2.3) (2020-07-04)


### Documentations

* update with Weather and UV-Index for July 03, 2020 ([9157b68](https://github.com/ALT-F1/OpenWeatherMap/commit/9157b68051030792171d9f7b6e9643debc892005))

### [3.2.2](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.1...v3.2.2) (2020-07-03)


### Documentations

* update with Weather and UV-Index for July 02, 2020 ([b3c7d47](https://github.com/ALT-F1/OpenWeatherMap/commit/b3c7d477208820f38f88fe61760b4969117b0adf))

### [3.2.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.2.0...v3.2.1) (2020-07-02)


### Documentations

* update with Weather and UV-Index for July 01, 2020 ([d57f3f6](https://github.com/ALT-F1/OpenWeatherMap/commit/d57f3f632297181345fc668d2f926ab3383d9b80))

## [3.2.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.9...v3.2.0) (2020-07-01)


### Features

* create a shell script that activates conda/pandas environment and create the dataset containing the temperature and the UV-Index of Belgian provinces ([c461812](https://github.com/ALT-F1/OpenWeatherMap/commit/c4618126d1d04e7be53b7446486263c534694fb7))

### [3.1.9](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.8...v3.1.9) (2020-07-01)


### Documentations

* update with Weather and UV-Index for July 01, 2020 ([983de2c](https://github.com/ALT-F1/OpenWeatherMap/commit/983de2c2bdb2dd622001a3987d187e625792dc87))

### [3.1.8](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.7...v3.1.8) (2020-06-30)


### Documentations

* update with Weather and UV-Index for June 29, 2020 ([7311bcf](https://github.com/ALT-F1/OpenWeatherMap/commit/7311bcfb1d7d06e8f108af76d787fa9a2b08d260))


### Chores

* **release:** 3.1.8 ([c78a1d8](https://github.com/ALT-F1/OpenWeatherMap/commit/c78a1d8c4a9367cca398b97c4242f24b99d00bd3))

### [3.1.7](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.6...v3.1.7) (2020-06-29)


### Documentations

* update with Weather and UV-Index for June 27 and 28, 2020 ([0e14f06](https://github.com/ALT-F1/OpenWeatherMap/commit/0e14f0646ee28bac08b43a04d301bbeea2c0a889))

### [3.1.6](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.5...v3.1.6) (2020-06-27)


### Documentations

* update with Weather and UV-Index for June 26, 2020 ([3902965](https://github.com/ALT-F1/OpenWeatherMap/commit/39029652e0a35c068fff9c08fb95c967794c93cb))

### [3.1.5](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.4...v3.1.5) (2020-06-26)


### Documentations

* update with Weather and UV-Index for June 24, 2020 ([c2cf96f](https://github.com/ALT-F1/OpenWeatherMap/commit/c2cf96ff9e6bfc718c184d425a9f6f8adbf6e849))
* update with Weather and UV-Index for June 25, 2020 ([a472045](https://github.com/ALT-F1/OpenWeatherMap/commit/a472045f6a8ad7bfadb74709f3fc49c3e65c7b3c))

### [3.1.4](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.3...v3.1.4) (2020-06-25)

### [3.1.3](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.2...v3.1.3) (2020-06-23)


### Documentations

* update with Weather and UV-Index for June 22, 2020 ([ee60b47](https://github.com/ALT-F1/OpenWeatherMap/commit/ee60b47b0a441ee6cd389ba9816c6ae0d7df61ab))

### [3.1.2](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.1...v3.1.2) (2020-06-22)


### Documentations

* update with Weather and UV-Index for June 22, 2020 ([f96042b](https://github.com/ALT-F1/OpenWeatherMap/commit/f96042b551f18f149bdc7277c518a98c2a71d828))

### [3.1.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.1.0...v3.1.1) (2020-06-21)


### Documentations

* update with Weather and UV-Index for June 20, 2020 ([10cec53](https://github.com/ALT-F1/OpenWeatherMap/commit/10cec536beb7ba6a9d3cf936eb30c633c56bf734))

## [3.1.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.0.3...v3.1.0) (2020-06-20)


### Documentations

* update with Weather and UV-Index for June 19, 2020 ([601c5d0](https://github.com/ALT-F1/OpenWeatherMap/commit/601c5d01ca44aea1f980749115be34514f7e500e))

### [3.0.3](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.0.2...v3.0.3) (2020-06-18)


### Documentations

* update with Weather and UV-Index for June 17, 2020 ([bfa85e0](https://github.com/ALT-F1/OpenWeatherMap/commit/bfa85e02dfbbeed1dd063e994ab6df84ebc04874))

### [3.0.2](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.0.1...v3.0.2) (2020-06-17)


### Documentations

* update with Weather and UV-Index for June 16, 2020 ([3d23ce4](https://github.com/ALT-F1/OpenWeatherMap/commit/3d23ce4577a3093b63604a21eca3fc35424f2b76))


### Chores

* ignore output_directory/data/{BPost.be}{by_date}{by_province_and_quartile}{OpenWeatherMap.org} and do not send 160.000+ files on github ([94948d1](https://github.com/ALT-F1/OpenWeatherMap/commit/94948d177259397d9d37c63c83ade7047eec0d2b))

### [3.0.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v3.0.0...v3.0.1) (2020-06-16)


### Documentations

* add info regarding the UV-Index; add the link to the aggregated version of the Weather data and UV-Index ([72b836c](https://github.com/ALT-F1/OpenWeatherMap/commit/72b836cd0faa50578d9fbc555e8ce62aab9bba47))

## [3.0.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.1.1...v3.0.0) (2020-06-16)


### Features

* add method date_utc to set the timezone to UTC ([db6fb98](https://github.com/ALT-F1/OpenWeatherMap/commit/db6fb983c94c51e17f5a3c37506cbe52a83285eb))
* add stdout logs, remove unnecessary logging, fix name of the log file ([763c503](https://github.com/ALT-F1/OpenWeatherMap/commit/763c50367067d2e479e63add74f6e1bc404e9a09))
* GET commands are performed in a Session including a retry mechanism; add hide_secrets_from_url to do not display sensitive data into the logs ([5ddb821](https://github.com/ALT-F1/OpenWeatherMap/commit/5ddb8214478550610854b17dc6020805b517d08c))
* store the UV-Index from OpenWeatherMap.org, force the timezone to UCT ([2196b19](https://github.com/ALT-F1/OpenWeatherMap/commit/2196b1996e1c04cda077f89666f66fc206939a49))
* use the Session/retry mechanism to improve the realibity of the download of data from OpenWeatherMap ([f77e1e1](https://github.com/ALT-F1/OpenWeatherMap/commit/f77e1e1093925c51bceb64f8d22b0f97bf01c983))


### Bug Fixes

* force the timezone to UTC ([2ab52cf](https://github.com/ALT-F1/OpenWeatherMap/commit/2ab52cfb8a707c007f836cb780d916f083de2427))
* set the timezone to UTC ([052ab04](https://github.com/ALT-F1/OpenWeatherMap/commit/052ab049ce8ccfc97e7a714d81a8587f3a18f277))


### Chores

* .gitignore the directory containing snippets of code ([586577f](https://github.com/ALT-F1/OpenWeatherMap/commit/586577f633ec11a1bf762c231eeb1b792f688a63))


### Builds

* add new libraries to requirements.txt ([8633e3f](https://github.com/ALT-F1/OpenWeatherMap/commit/8633e3f9b68f179464bcb1c62416a4090ff6b8e8))


### Documentations

* remove intermediate files ([a92c0c0](https://github.com/ALT-F1/OpenWeatherMap/commit/a92c0c05f1f846726ca7b02964a7b4615f6a0050))
* update up to June 15, 2020 ([2786a25](https://github.com/ALT-F1/OpenWeatherMap/commit/2786a2596d40d7d76f0af5f103572f1004817cf3))

### [2.1.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.1.0...v2.1.1) (2020-06-14)


### Documentations

* the application collects the raw AV-Index from OpenWeatherMap.org ([b0caf79](https://github.com/ALT-F1/OpenWeatherMap/commit/b0caf799a9207e13b6640662e7998ff45b65f573))

## [2.1.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.10...v2.1.0) (2020-06-14)


### Features

* get Ultraviolet index UV-Inde from openweathermap.org, add logging stored in output_directory/logs/eosc-gees-weather_in_belgian_provinces_per_day.py.log ([24bc4a0](https://github.com/ALT-F1/OpenWeatherMap/commit/24bc4a09f2dea6f9ff912c88ffcf978fe7ddd52c))
* get Ultraviolet index UV-Inde from openweathermap.org, add logging stored in output_directory/logs/openweathermap_helpers.py.log ([d343c10](https://github.com/ALT-F1/OpenWeatherMap/commit/d343c1024db3c89e943c43fbe77c4be42c65383c))


### Documentations

* add UV-Index for all cities in Belgium from March 01,2020 to June 13, 2020 ([259f453](https://github.com/ALT-F1/OpenWeatherMap/commit/259f4532ab310b518065e55633b96cda268eb90c))

### [2.0.10](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.9...v2.0.10) (2020-06-14)


### Documentations

* add weather for June 13, 2020 ([0d47bfb](https://github.com/ALT-F1/OpenWeatherMap/commit/0d47bfb57ae342c5078fa60f7b2bc393f744a995))

### [2.0.9](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.8...v2.0.9) (2020-06-13)


### Features

* altf1be_helpers uses atatic methods. Add valid_time, valid_date, count_files_in_dir, input_directory methods ([5658e35](https://github.com/ALT-F1/OpenWeatherMap/commit/5658e358b421b49a27ddbbb00ef328ab5872a694))
* do not display the secret api in the logs anymore ([64f60bd](https://github.com/ALT-F1/OpenWeatherMap/commit/64f60bd835134de4eba42d4722224cf41eda3674))


### Refactorings

* BPost_postal_codes class uses the static methods of altf1be_helpers ([a5a42f2](https://github.com/ALT-F1/OpenWeatherMap/commit/a5a42f2919072deefef082f5955bb798bb8790e7))


### Documentations

* add aggregate data up to June 12, 2020 ([23a93d5](https://github.com/ALT-F1/OpenWeatherMap/commit/23a93d570c02986a9a36627c76b4f071693fea60))
* add weather June 11 and June 12, 2020 ([eae2455](https://github.com/ALT-F1/OpenWeatherMap/commit/eae24557baa86dedf85ff459c32492265f837aa8))
* add weather June 11, 2020 ([afaf36c](https://github.com/ALT-F1/OpenWeatherMap/commit/afaf36c2e7a931849d18f1218e487c1692ff85e5))
* add weather June 11, 2020 ([f5aba79](https://github.com/ALT-F1/OpenWeatherMap/commit/f5aba794c0a6c9083c79df36e6336b04c0b8811b))
* add weather June 12, 2020 ([a7cbb7e](https://github.com/ALT-F1/OpenWeatherMap/commit/a7cbb7e7b6959022bfce02f268bac21016830447))

### [2.0.8](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.7...v2.0.8) (2020-06-11)


### Chores

* set the LICENSE to EUROPEAN UNION PUBLIC LICENCE v. 1.2 ([17fa084](https://github.com/ALT-F1/OpenWeatherMap/commit/17fa084fe6f32599eb8e0581ab4c71fcf854b31b))


### Documentations

* add sub-headings to improve the visibility of the sponsors, add info about ALT-F1 ([2fdefe2](https://github.com/ALT-F1/OpenWeatherMap/commit/2fdefe22566ac49cb88cf386dc095c9513aa6100))

### [2.0.7](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.6...v2.0.7) (2020-06-11)


### Bug Fixes

* [#1](https://github.com/ALT-F1/OpenWeatherMap/issues/1) update the generation of 3 dates: June 08-09-10, 2020 ([ea3d5cf](https://github.com/ALT-F1/OpenWeatherMap/commit/ea3d5cf5a20b53e6c5f4749a55cc35f319d67a45))

### [2.0.6](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.5...v2.0.6) (2020-06-11)


### Features

* add new weather data from June 01, 2020 ([5e7a16c](https://github.com/ALT-F1/OpenWeatherMap/commit/5e7a16c2c5fb5c31a62a130f6b0352739d213546))


### Documentations

* add June 08-09-10, 2020 ([b8f014e](https://github.com/ALT-F1/OpenWeatherMap/commit/b8f014e8bdf530ba2aad2647e71b45d040750f47))

### [2.0.5](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.4...v2.0.5) (2020-06-08)


### Styles

* update the picture containing the logo of the contributors to the project ([0fc761d](https://github.com/ALT-F1/OpenWeatherMap/commit/0fc761db0d00ddc0d752d2e7a727d327bcc882f7))

### [2.0.4](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.3...v2.0.4) (2020-06-08)


### Documentations

* correct grammar and misspellings ([cdcdc1e](https://github.com/ALT-F1/OpenWeatherMap/commit/cdcdc1ef95dbfb0870e72cc2274fb2b33194c64d))

### [2.0.3](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.2...v2.0.3) (2020-06-08)


### Documentations

* add the section '# What are the data available?', rename a heading and the title ([913507e](https://github.com/ALT-F1/OpenWeatherMap/commit/913507e91cf64c6ede61187b1c1b7c877e700f99))

### [2.0.2](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.1...v2.0.2) (2020-06-08)


### Documentations

* remove some text ([98507e2](https://github.com/ALT-F1/OpenWeatherMap/commit/98507e2d8fc1de193b32740367c55995048284b8))

### [2.0.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v2.0.0...v2.0.1) (2020-06-08)


### Chores

* add first time the weather data compiled in one file, from 2020-03-01 to 2020-06-07 under output_directory/data/latest/by_province_and_quartile.csv ([c730dc5](https://github.com/ALT-F1/OpenWeatherMap/commit/c730dc5024662f368042478387be47cd68a3fcd6))

## [2.0.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.4.2...v2.0.0) (2020-06-08)


### Features

* create one CSV file containing all dates for the weather stored in output_directory/data/by_province_and_quartile ([78836da](https://github.com/ALT-F1/OpenWeatherMap/commit/78836dadc328a53252f60fb845b75460afc72ca1))

### [1.4.2](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.4.1...v1.4.2) (2020-06-08)


### Chores

* remove files that were under export_from_openweathermap directory due to a change in the tree structure ([b46ac62](https://github.com/ALT-F1/OpenWeatherMap/commit/b46ac62a6f3c3a8204b6026d06ad0bde420e60f2))
* remove the first version of the generation of the weather data ([7a79526](https://github.com/ALT-F1/OpenWeatherMap/commit/7a79526ab2860da37c86b54f0b248eb15d2dac63))

### [1.4.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.4.0...v1.4.1) (2020-06-08)


### Chores

* add weather data per day collected from OpenWeatherMap.org and add aggregated weather data by province including the quantiles 25-50-75 ([1807ae8](https://github.com/ALT-F1/OpenWeatherMap/commit/1807ae86b311de3c8207d6d74a0cb2f71747d62b))
* delete useless historical data since all output data are stored in output_directory ([09d1099](https://github.com/ALT-F1/OpenWeatherMap/commit/09d1099ff1d098c88c781e32e536f3b1cbd8a904))

## [1.4.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.3.2...v1.4.0) (2020-06-08)


### Features

* add a method: translate Belgian provinces in French ([33ffbc0](https://github.com/ALT-F1/OpenWeatherMap/commit/33ffbc062814d0632077577432180733dbea215f))
* create a directory by_date/yyy-mm-dd/*.{json|csv} containing OpenWeatherMap.org for Belgian cities, and group the weather data by province, and add quantiles 25-50-75 for main.temp, main.feels_like, main.pressure, main.humidity, main.temp, wind.speed, wind.deg ([4da4114](https://github.com/ALT-F1/OpenWeatherMap/commit/4da4114e7ccf53c3b26d35b548c1b4eebe955f46))


### Documentations

* add further method' documentation based on python3 best practice ([a8a3a95](https://github.com/ALT-F1/OpenWeatherMap/commit/a8a3a9502498b1ff7dc8d6df0dc49e833fb2f0f5))
* add further method' documentatoin ([0d8ff91](https://github.com/ALT-F1/OpenWeatherMap/commit/0d8ff917f833fa5c9085d366cb10161309cba6ed))

### [1.3.2](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.3.1...v1.3.2) (2020-06-07)


### Refactorings

* rename the library to bpost_be_postal_code_helpers.py ([d9d2130](https://github.com/ALT-F1/OpenWeatherMap/commit/d9d2130a2d956ecf4cf3ea8786344d61c9aadb40))


### Styles

* remove the github social preview made with Pinta ([7172453](https://github.com/ALT-F1/OpenWeatherMap/commit/7172453e42cd1e0f26234454a351ad1353546f5f))

### [1.3.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.3.0...v1.3.1) (2020-06-07)


### Chores

* delete input files moved under the same directory as it was stored on Kaggle ([84962a5](https://github.com/ALT-F1/OpenWeatherMap/commit/84962a5cae39104305f1f7b2a96ec58b2abe312e))
* delete unused files ([765cb0a](https://github.com/ALT-F1/OpenWeatherMap/commit/765cb0a30d5ca18b2adde66a04124d493e6cdfc2))


### Refactorings

* add get_history method to download the OpenWeatherMap.org, load history city list like it is stored on kaggle, add build_df method to separate the initialization of properties and build the DataFrame ([0bbdc73](https://github.com/ALT-F1/OpenWeatherMap/commit/0bbdc7321598004919efbf741d85d6ce97a9679c))
* create a class to ease the use of the methods, output_directory use the separator / or \ depending on the operating system ([06e0d42](https://github.com/ALT-F1/OpenWeatherMap/commit/06e0d42845c9c851b7d78db1f86601885a0766c4))
* move the file in the same directory as it was stored on Kaggle ([a8e15f5](https://github.com/ALT-F1/OpenWeatherMap/commit/a8e15f5b3a601b2c8ce30030ae83378fc98c3954))
* use AltF1BeHelpers class instead of functions ([aa68849](https://github.com/ALT-F1/OpenWeatherMap/commit/aa688496a9a6889095760236f27f461a5daa41e2))
* use AltF1BeHelpers class instead of functions, move openweathermap_get_history into openweathermap_helpers.py, add usage example of the class ([dd25155](https://github.com/ALT-F1/OpenWeatherMap/commit/dd25155ca118c5a58774022f57e9b5e564a199ed))

## [1.3.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.2.1...v1.3.0) (2020-06-06)


### Features

* altf1be_helpers.py checks if you use an interactive app such as kaggle, convert unicode to ascii, set the output directory and set a date range ([966bfdc](https://github.com/ALT-F1/OpenWeatherMap/commit/966bfdcac11109de89dc50d0e5136bcb816bcd1c))
* BPost_postal_codes extract-transform-load postal codes metadata from bpost.be to manipulate them ([899af49](https://github.com/ALT-F1/OpenWeatherMap/commit/899af49903b8c4996d02b5cad28a62b185c2f644))
* merge OpenWeatherMap.org and BPost.be cities and postal codes and store them under output_directory/data/YYY-MM-DD directories ([1237115](https://github.com/ALT-F1/OpenWeatherMap/commit/123711578b54673a6f265f28b8e9cb28c346f1b4))
* OpenWeatherMap class extract-transform-load cities from OpenWeatherMap.org to manipulate them ([069e2ff](https://github.com/ALT-F1/OpenWeatherMap/commit/069e2ff07748f24705ce3fdf4b88583f228d60f5))


### Bug Fixes

* rename create_pd to csv_to_df to facilitate the understanding ([b3c5bea](https://github.com/ALT-F1/OpenWeatherMap/commit/b3c5bea15e918ffd87d43475409f2a12cd5763db))


### Documentations

* add 'how to run the code' section ([a11a44e](https://github.com/ALT-F1/OpenWeatherMap/commit/a11a44e51843cbe6b3b50109f213ec3d76f36adf))
* correct misspellings ([7509ea5](https://github.com/ALT-F1/OpenWeatherMap/commit/7509ea5d3386859ce95739e60623cdf0b0a1d2f3))


### Chores

* add .json and .csv in .gitattributes ([d6db11a](https://github.com/ALT-F1/OpenWeatherMap/commit/d6db11a9f4315cd9a72f87fef2d3bc8b4c6e8a69))
* store BPost.be postal codes ([e987090](https://github.com/ALT-F1/OpenWeatherMap/commit/e987090564011461cbfb2d4625f4ca8c732252f6))


### Styles

* add github social preview ([bbe9a1c](https://github.com/ALT-F1/OpenWeatherMap/commit/bbe9a1cd223d4e818ac5beaefc12be5476b20030))

### [1.2.1](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.2.0...v1.2.1) (2020-05-24)


### Features

* store temporary files (dataframes) under a 'tmp' directory ([7504cf9](https://github.com/ALT-F1/OpenWeatherMap/commit/7504cf9edcc18dd1ba08964d5486dc4ef4338075))

## [1.2.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.1.0...v1.2.0) (2020-05-24)


### Features

* generate the weather data in Belgium from March 01, 2020 to May 22, 2020 in csv format ([218133f](https://github.com/ALT-F1/OpenWeatherMap/commit/218133f7d42aee42973f452056556ca5e37ac59f))


### Chores

* add src/data/export_from_openweathermap/historical/csv-March_2020-to-May_22_2020 to gitignore ([85737be](https://github.com/ALT-F1/OpenWeatherMap/commit/85737be6b0b13d3ba75ec5b12dd7540ac6dc0da9))

## [1.1.0](https://github.com/ALT-F1/OpenWeatherMap/compare/v1.0.0...v1.1.0) (2020-05-22)


### Features

* export openweathermap.org historical data in json ([d7175a8](https://github.com/ALT-F1/OpenWeatherMap/commit/d7175a899b16ff50b02d99a192b35e9c6bb9e8da))

## 1.0.0 (2020-05-21)


### Features

* create a dataframe containing the cities and postal codes, then get the weather data for each city ([55e6678](https://github.com/ALT-F1/OpenWeatherMap/commit/55e6678ce5b08d0a402917601062239c3be4ce06))


### Documentations

* describe the project in details ([b470429](https://github.com/ALT-F1/OpenWeatherMap/commit/b470429f1c5c835f86339d9a52299794d694f8f5))
* set the license to alt-f1.be ALT-F1 SPRL ([c3c8a39](https://github.com/ALT-F1/OpenWeatherMap/commit/c3c8a3937178511250b1ab93fe0ef409e0cdb958))
* store the belgian postal codes in French and Dutch ([a600d05](https://github.com/ALT-F1/OpenWeatherMap/commit/a600d05cabc537bd61b46891cf84744532989d5a))
* store the cities list recognized by openweathermap.org ([31ff5d1](https://github.com/ALT-F1/OpenWeatherMap/commit/31ff5d1a41190995d372adf77a006f824ae8a338))
* store the weather data from cities in Belgium collected from openweathermap.org ([4584a24](https://github.com/ALT-F1/OpenWeatherMap/commit/4584a24b9053ddbb68092520e86431d6483eded0))
* store the weather data from cities in Belgium collected from openweathermap.org ([cfcd6ad](https://github.com/ALT-F1/OpenWeatherMap/commit/cfcd6add9be2db9146c8e582ba67f1d9d8d9e433))
* store the weather data from cities in Belgium collected from openweathermap.org ([046f314](https://github.com/ALT-F1/OpenWeatherMap/commit/046f3149cd28d32832d24d20018501b3518d1558))


### Builds

* define the configuration of the project ([d72cc45](https://github.com/ALT-F1/OpenWeatherMap/commit/d72cc4580d080b43fb22fe20e86e640074d6b5f2))
* set the libraries required to run the code ([4f05630](https://github.com/ALT-F1/OpenWeatherMap/commit/4f0563021fadb42ce3fe5350e3f31289651fce17))
* set the local environment with conda named 'pandas' ([f1d4e31](https://github.com/ALT-F1/OpenWeatherMap/commit/f1d4e31ad0396513a3e9e941c1f0f281098da730))


### Chores

* add .gitignore matching those file types: node,python,jupyternotebooks,visualstudiocode ([48055b1](https://github.com/ALT-F1/OpenWeatherMap/commit/48055b1fb2a99e0a0affad85c8b3a9d2a8f40abe))
* add custom .gitattributes ([0a884f2](https://github.com/ALT-F1/OpenWeatherMap/commit/0a884f2ed26d279446988e8a3848edc06c564b58))
* set the .gitignore for node, python, jupyternotebooks, visualstudiocode ([9a3dba1](https://github.com/ALT-F1/OpenWeatherMap/commit/9a3dba19f50650e07094890999df756e3b0bac79))
* set the debug configuration ([c4c92a3](https://github.com/ALT-F1/OpenWeatherMap/commit/c4c92a3244f5fa9bad146abca22a1e08f9d27d8a))
* set the visible headers in the CHANGELOG ([fb07a72](https://github.com/ALT-F1/OpenWeatherMap/commit/fb07a7213a2f478b8398e0be358857461f194762))
